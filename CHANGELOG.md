# Changelog

## v1.1.0()-修复若干 bug

* redis 插件实现默认二进制存储和 json 存储
* aspectj 注解插件，单独拆分认证注解切面实现
* 去掉例子中的 Log4j2依赖

## v1.0.9(20211210)-升级到 springboot 2.6.1

* 升级 springboot 版本到2.6.1
* 解决配置文件注入导致的循环依赖问题

## v1.0.8(20211020)-实现定时清理任务和 thymeleaf 方言支持

* 基于 guava 实现定时token 缓存和权限缓存自动清理任务
* 实现 thymeleaf 权限方言支持。同时支持 attribute 和 element 的权限控制。
* 实现基于 webflux 、Bearer token 认证授权的应用示例
* 完善 solon 和 jfinal 的应用示例
* 完善 servlet + session+cookie 的应用示例，含在线用户管理和 thymeleaf 权限方言使用。

## v1.0.7(20211010)-全面改版支持更多功能

* 重构项目结构
* 新增多个 plugins 和 starters，主要包括 jwt、cache等
* 支持 Session+cookie 或者 Jwt token 模式认证
* 实现基于Yaml配置文件的用户权限信息配置和解析
* Servlet 和 webflux 容器支持
* Solon 框架和 JFinal 框架支持
* 内存缓存和 Redis 缓存支持和优化
* 新增更多应用示例

## v1.0.6(20210915)-thymeleaf 支持，统一依赖版本管理

* 实现 thymeleaf 权限标签
* 第三方依赖版本统一根 pom 管理
* 完善示例应用

## v1.0.5(20210129)-在线用户总数逻辑优化

* 调整获取在线用户总数的方法，改成通过实际 SessionId 缓存获取

## v1.0.4(20210129)-调整SpringBoot Starter默认实现

* 调整 SpringBoot Starter ，去除特定业务依赖，只保留 Heimdall 框架相关功能。
* 移除全局异常处理实现，由 Sample 示例程序实现
* 移除全局异常 Controller实现，由 Sample 示例程序实现
* resolver 依赖由AutoWired调整为构造函数注入
* 移除 Heimdall 配置参数日志输出

## v1.0.3(20210124) -优化默认配置

* 取消CurrentUserRequestArgumentResolver自动注册，改为手动注册
* 优化 SessionDao 构造方式，去掉 CookieService
* 修复用户权限缓存重复清理两次的 bug
* 将 Cookie功能 默认开启 修改为: 默认关闭

## v1.0.2 (20210120) -新增和优化功能

* 本地 MapCache 缓存，不支持过期策略，废弃
* 实现 Caffeine Session 缓存和权限缓存
* 实现用户权限单独缓存,便于动态授权
* 对已登录用户的 Cookie 写入机制进行优化,解决刷新页面后 Cookie 丢失问题
* 功能演示示例修改，实现三种认证授权方式和数据处理逻辑
* 配置参数修改，将系统权限缓存配置独立成单独的配置文件。见:AuthorityProperty
* 新增Session配置参数:concurrentLogin，用以设置重复登录的处理行为。
* 修改登录事件监听逻辑，新增对重复登录不同状态的处理。0:踢出前者 1:拒绝后者 2:正常登录
* 修改 redis 缓存配置参数，去掉事务支持。
* 错误页配置参数修正:可通过server.error.name或者error.name指定错误页面的名称，默认:error
* com.luter.heimdall.cache.caffeine包名错误修正，原包:....caffeinel
* 实现认证错误重试次数限定功能
