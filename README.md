# Heimdall

>&emsp;&emsp;**Heimdall** Java Web 认证授权框架，类似于Spring Security 和shiro,实现认证和授权功能. 相比之下,Heimdall框架更加简单，也更容易上手。 支持Session
Cookie 、Jwt认证方式，支持普通Url权限和Restful资源权限， 
> 支持 Servlet 、 Spring WebFlux 
> 支持内存缓存和redis缓存。 同时提供了主流 web 框架的Starter，便于快速集成。

## 技术选型

##### 开发环境

* Maven 3
* Git 2
* JDK 1.8

## 主要功能

* Session+cookie 和 Jwt token 两种模式
* 多应用(app)多用户(user)认证授权体系支持
* 多容器支持:支持 servlet 和 spring webflux
* 基于 Cookie 实现的'记住我'功能
* 用户重复登录限制
* 登录密码重试锁定
* 在线会话管理:在线用户、强制下线等。
* jwt 白名单
* 会话续签
* 内存缓存(Map,guava,Caffeine)和 外部缓存(redis)支持
* thymeleaf 权限方言支持
* 缓存自动清理任务
* 注解授权:基于拦截器或者 aspectJ 实现
* 路由授权：支持普通精确路由和 Restful 路由授权
* 多权限数据源支持：默认实现基于配置文件的用户和权限数据提供者服务
* 认证授权事件通知
* 多框架脚手架支持：支持 JFinal，Spring，SpringBoot，Solon 框架快速集成

- - -

## 开发

1. 从 Gitee 或者 github clone 代码到本地
2. 在根pom.xml所在目录执行 mvn clean package

## 交流反馈,参与贡献

- Github

<a target="_blank" href="https://github.com/luterc/heimdall">**Heimdall 框架**</a>

**技术交流QQ群:    554290469**

欢迎fork，star，欢迎提需求，欢迎吐槽，支持共建！

## License

Apache License, Version 2.0

