/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.core.annotation;

import java.lang.annotation.*;

/**
 * 需要一个或者多个权限标识才能访问
 * <p>
 * <p>
 * 前置条件: 需要登录
 * <p>
 * eg:
 * #@RequiresPermissions(value = {"catList", "catDetail"}, logical = Logical.ANY)
 *
 * @author luter
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
@RequiresUser
public @interface RequiresPermissions {
    /**
     * 需要验证的权限标识
     *
     * @return the string [ ]
     * #@RequiresPermissions(value = {"catList", "catDetail"}, logical = Logical.ANY)
     */
    String[] value() default {};

    /**
     * 指定验证模式是AND还是OR，默认:ANY
     *
     * @return 验证模式 logical
     */
    Logical logical() default Logical.ANY;

}
