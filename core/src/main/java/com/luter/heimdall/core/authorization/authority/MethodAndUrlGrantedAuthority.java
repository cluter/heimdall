/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.core.authorization.authority;


import com.luter.heimdall.core.utils.HttpMethodEnum;
import com.luter.heimdall.core.utils.PathUtil;
import com.luter.heimdall.core.utils.StrUtils;

import java.util.Objects;

/**
 * Restful  风格路径的权限匹配标识定义
 * <p>
 * 格式：Method:URL
 * <p>
 * 如：GET:/users/1、GET:/users
 * <p>
 * 则，权限规则为:/users/* 、 /users
 * <p>
 * 资源method+url 全局唯一
 *
 * @author luter
 */
public class MethodAndUrlGrantedAuthority implements GrantedAuthority {
    /**
     * The constant serialVersionUID.
     */
    private static final long serialVersionUID = 4484435462234826666L;
    /**
     * 请求方法
     */
    private String method;

    /**
     * url路径
     */
    private String url;

    /**
     * Instantiates a new Method and url granted authority.
     */
    public MethodAndUrlGrantedAuthority() {
    }

    /**
     * Instantiates a new Method and url granted authority.
     *
     * @param method the method
     * @param url    the url
     */
    public MethodAndUrlGrantedAuthority(String method, String url) {
        if (!HttpMethodEnum.isValidMethod(method)) {
            throw new IllegalArgumentException("Illegal Argument of method :[" + method + "],Invalid Method Name.");
        }
        if (PathUtil.isInValidPath(url)) {
            throw new IllegalArgumentException("Illegal Argument of url :[" + url + "],Invalid url format.");
        }
        this.method = method;
        this.url = url;
    }

    /**
     * method:url形式组合
     */
    @Override
    public String getAuthority() {
        return this.method + StrUtils.COLON + this.url;
    }

    /**
     * Gets method.
     *
     * @return the method
     */
    public String getMethod() {
        return method;
    }

    /**
     * Sets method.
     *
     * @param method the method
     * @return the method
     */
    public MethodAndUrlGrantedAuthority setMethod(String method) {
        this.method = method;
        return this;
    }

    /**
     * Gets url.
     *
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets url.
     *
     * @param url the url
     * @return the url
     */
    public MethodAndUrlGrantedAuthority setUrl(String url) {
        this.url = url;
        return this;
    }

    @Override
    public String toString() {
        return "MethodAndUrlGrantedAuthority{" +
                "method='" + method + '\'' +
                ", url='" + url + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MethodAndUrlGrantedAuthority that = (MethodAndUrlGrantedAuthority) o;
        return method.equals(that.method) && url.equals(that.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(method, url);
    }
}