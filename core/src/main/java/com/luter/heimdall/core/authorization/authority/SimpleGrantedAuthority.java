/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.core.authorization.authority;


import com.luter.heimdall.core.utils.StrUtils;

import java.util.Objects;

/**
 * 权限标识符授权实体
 * <p>
 * 如：
 * <p>
 * 资源：/user/save,attr="Perm"
 * <p>
 * 资源：/user/list,attr="Role"
 * <p>
 * 一个唯一 Url 资源，对应一个唯一 Attr 权限 标识符
 * <p>
 * 资源url 全局唯一，不区分请求 method
 *
 * @author Luter
 */
public class SimpleGrantedAuthority implements GrantedAuthority {
    /**
     * The constant serialVersionUID.
     */

    private static final long serialVersionUID = -8133992548830905448L;
    /**
     * 权限标志，或者角色标志。
     */
    private String attr;

    /**
     * Instantiates a new Simple granted authority.
     */
    public SimpleGrantedAuthority() {
    }

    /**
     * Instantiates a new Simple granted authority.
     *
     * @param attr 权限标志 或者角色标志
     */
    public SimpleGrantedAuthority(String attr) {
        if (StrUtils.isBlank(attr)) {
            throw new IllegalArgumentException("Illegal Argument of attr :[" + attr + "],It can not be null.");
        }
        this.attr = attr;
    }

    @Override
    public String getAuthority() {
        return this.attr;
    }

    /**
     * Sets authority.
     */
    public void setAuthority() {
    }

    /**
     * Gets attr.
     *
     * @return the attr
     */
    public String getAttr() {
        return attr;
    }

    /**
     * Sets attr.
     *
     * @param attr the attr
     * @return the attr
     */
    public SimpleGrantedAuthority setAttr(String attr) {
        this.attr = attr;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SimpleGrantedAuthority that = (SimpleGrantedAuthority) o;
        return attr.equals(that.attr);
    }

    @Override
    public int hashCode() {
        return Objects.hash(attr);
    }

    @Override
    public String toString() {
        return "SimpleGrantedAuthority{" +
                "attr='" + attr + '\'' +
                '}';
    }
}
