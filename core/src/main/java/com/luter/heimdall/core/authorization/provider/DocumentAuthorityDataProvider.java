/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.core.authorization.provider;

import com.luter.heimdall.core.authorization.authority.GrantedAuthority;
import com.luter.heimdall.core.details.UserDetails;
import org.slf4j.Logger;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * 基于配置文件的权限管理控制
 *
 * @author luter
 */
public class DocumentAuthorityDataProvider implements AuthorityDataProvider {
    /**
     * The constant log.
     */
    private static final transient Logger log = getLogger(DocumentAuthorityDataProvider.class);

    @Override
    public Map<String, List<String>> loadAppAuthorities(String appId) {
        log.warn("[DocumentAuthorityDataProvider->loadAppAuthorities]::appId = [{}]", appId);
        return DocumentAuthorizationParser.parseAppPerms(appId);
    }

    @Override
    public List<? extends GrantedAuthority> loadUserAuthorities(UserDetails userDetails) {
        log.warn("[DocumentAuthorityDataProvider->loadUserAuthorities]::userDetails = [{}]", userDetails);
        return DocumentAuthorizationParser.parseUserPerms(userDetails.getAppId(), userDetails.getUserId());
    }
}
