/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.core.authorization.provider;

import com.luter.heimdall.core.authorization.authority.GrantedAuthority;
import com.luter.heimdall.core.details.UserDetails;
import org.slf4j.Logger;

import java.util.*;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * 默认 无权限数据实现
 *
 * @author luter
 */
public class NoneAuthoritiesDataProvider implements AuthorityDataProvider {
    private static final transient Logger log = getLogger(NoneAuthoritiesDataProvider.class);

    @Override
    public Map<String, List<String>> loadAppAuthorities(String appId) {
        log.warn("[loadAppAuthorities]:: ATTENTION==> You have not implemented the application authorization data provider," +
                " and all of resources access will be passed。 appId = [{}]", appId);
        return new HashMap<>(0);
    }

    @Override
    public List<? extends GrantedAuthority> loadUserAuthorities(UserDetails userDetails) {
        log.warn("[loadUserAuthorities]::ATTENTION==> You have not implemented the user authorization data provider " +
                " and all of resources access will be denied. userDetails = [{}]", userDetails);
        return new LinkedList<>();
    }
}
