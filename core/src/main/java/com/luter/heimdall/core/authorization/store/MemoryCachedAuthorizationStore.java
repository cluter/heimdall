/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.core.authorization.store;

import com.luter.heimdall.core.authorization.authority.GrantedAuthority;
import com.luter.heimdall.core.authorization.provider.AuthorityDataProvider;
import com.luter.heimdall.core.authorization.provider.NoneAuthoritiesDataProvider;
import com.luter.heimdall.core.cache.MapMemoryCache;
import com.luter.heimdall.core.cache.MemoryCache;
import com.luter.heimdall.core.config.ConfigManager;
import com.luter.heimdall.core.details.UserDetails;
import com.luter.heimdall.core.exception.HeimdallUnauthorizedException;
import com.luter.heimdall.core.token.SimpleToken;
import com.luter.heimdall.core.utils.StrUtils;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * 权限缓存 Store，基于 Map 的默认实现
 *
 * @author luter
 */
@Data
@Accessors(chain = true, fluent = true)
public class MemoryCachedAuthorizationStore implements AuthorizationStore {

    /**
     * The constant log.
     */
    private static final transient Logger log = getLogger(MemoryCachedAuthorizationStore.class);
    /**
     * 系统权限缓存
     */
    private MemoryCache<String, Map<String, List<String>>> appCache;
    /**
     * 用户权限缓存
     */
    private MemoryCache<String, List<? extends GrantedAuthority>> userCache;
    /**
     * 权限数据提供服务
     */
    private AuthorityDataProvider dataProvider;

    /**
     * 基础参数默认初始化。需要手动 set 缓存实现
     */
    public MemoryCachedAuthorizationStore() {
        this.dataProvider = new NoneAuthoritiesDataProvider();
        this.appCache = new MapMemoryCache<>(new ConcurrentHashMap<>(), "DEFAULT_MAP_APP_CACHE");
        this.userCache = new MapMemoryCache<>(new ConcurrentHashMap<>(), "DEFAULT_MAP_USER_CACHE");
    }

    /**
     * 默认 Map 缓存初始化
     *
     * @param dataProvider the metadata service
     */
    public MemoryCachedAuthorizationStore(AuthorityDataProvider dataProvider) {
        this();
        this.dataProvider = dataProvider;
    }

    /**
     * Instantiates a new Memory cached authorization store.
     *
     * @param appCache     the app cache
     * @param userCache    the user cache
     * @param dataProvider the metadata service
     */
    public MemoryCachedAuthorizationStore(MemoryCache<String, Map<String, List<String>>> appCache,
                                          MemoryCache<String, List<? extends GrantedAuthority>> userCache,
                                          AuthorityDataProvider dataProvider) {
        this();
        this.appCache = appCache;
        this.userCache = userCache;
        this.dataProvider = dataProvider;
    }

    @Override
    public void putUserAuthorities(String principal, List<? extends GrantedAuthority> authorities) {
        log.debug("[putUserAuthorities]::principal = [{}], authorities = [{}]", principal, authorities);
        if (StrUtils.isBlank(principal)) {
            throw new IllegalArgumentException("The User principal can not be null ");
        }
        if (null == authorities || authorities.isEmpty()) {
            log.warn("The authorities of principal:[{}] is empty and will not be cached", principal);
        } else {
            userCache.put(generateUserCacheKey(principal), authorities);
        }

    }

    @Override
    public boolean isSelfExpired() {
        return false;
    }

    @Override
    public List<? extends GrantedAuthority> getUserAuthorities(UserDetails userDetails) {
        //没登录，返回个空的权限列表
        if (null == userDetails || StrUtils.isBlank(userDetails.getPrincipal())) {
            return new ArrayList<>();
        }
        log.debug("[getUserAuthorities]::userDetails = [{}]", userDetails);
        String cacheKey = generateUserCacheKey(userDetails.getPrincipal());
        //从缓存 中获取
        List<? extends GrantedAuthority> cachedPerms = userCache.get(cacheKey);
        //缓存里没有
        if (null == cachedPerms || cachedPerms.isEmpty()) {
            //数据提供服务不为空
            if (null != dataProvider) {
                log.info("[getUserAuthorities]:: The authorities of User:[{}] loaded from Cache is empty. " +
                        "Now try to get it with MetaDataProvider ", userDetails.getPrincipal());
                cachedPerms = dataProvider.loadUserAuthorities(userDetails);
                //数据提供者里拿到的也是空的，用户就不具备任何权限
                if (null == cachedPerms || cachedPerms.isEmpty()) {
                    log.warn("The authorities of User:[{}] loaded from meta data service is empty", userDetails.getPrincipal());
                } else {
                    //数据提供者数据不为空，存入缓存
                    userCache.put(cacheKey, cachedPerms);
                    log.debug("The user authorities  is successfully " +
                            "loaded from the MetaDataService  " +
                            "and stored in the cache。user:[{}],authorities:[{}]", userDetails, cachedPerms);
                }
            } else {
                log.error("Authorization Metadata Service is not implemented. Failed to obtain user permissions");
            }

        }
        log.debug("The obtained User authorization data is: [{}]", cachedPerms);
        return cachedPerms;
    }

    @Override
    public void removeUserAuthorities(UserDetails userDetails) {
        if (null == userDetails || StrUtils.isBlank(userDetails.getPrincipal())) {
            throw new HeimdallUnauthorizedException("The User detail info can not be null");
        }
        if (StrUtils.isBlank(userDetails.getAppId())) {
            throw new IllegalArgumentException("appId can not be null");
        }
        if (StrUtils.isBlank(userDetails.getUserId())) {
            throw new IllegalArgumentException("userId can not be null");
        }
        log.debug("[removeUserAuthorities]::userDetails = [{}]", userDetails);
        final Collection<? extends GrantedAuthority> remove = userCache.remove(generateUserCacheKey(userDetails.getPrincipal()));
        log.info("[removeUserAuthorities]::remove UserAuthorities form cache. userDetails:[{}] Authorities:[{}]", userDetails, remove);
    }

    @Override
    public void removeUserAuthorities(String appId, String userId) {
        if (StrUtils.isNotBlank(appId) && StrUtils.isNotBlank(userId)) {
            log.debug("[removeUserAuthorities]::appId = [{}], userId = [{}]", appId, userId);
            removeUserAuthorities(new UserDetails(appId, userId));
        }
    }

    @Override
    public void removeAllUserAuthorities() {
        log.warn("[removeAllUserAuthorities]::");
        userCache.clear();
    }

    @Override
    public Map<String, List<String>> getAppAuthorities(String appId) {
        //校验一下 appId
        if (StrUtils.isBlank(appId)) {
            throw new IllegalArgumentException("The AppId can not be null");
        }
        log.debug("[getAppAuthorities]::appId = [{}]", appId);
        String cacheKey = generateAppCacheKey(appId);
        Map<String, List<String>> appPerms = appCache.get(cacheKey);
        if (null == appPerms || appPerms.isEmpty()) {
            if (null != dataProvider) {
                appPerms = dataProvider.loadAppAuthorities(appId);
                if (null == appPerms || appPerms.isEmpty()) {
                    log.warn("Authorities of appId:[{}] loaded from meta data service is empty", appId);
                } else {
                    appCache.put(cacheKey, appPerms);
                }
            } else {
                log.error("Authorization Metadata Service is not implemented. Failed to obtain system permission");
            }

        }
        log.debug("Obtained App authorization data is: [{}]", appPerms);
        return appPerms;
    }

    @Override
    public Map<String, List<String>> getAppAuthorities(UserDetails userDetails) {
        if (null == userDetails || StrUtils.isBlank(userDetails.getAppId())) {
            throw new HeimdallUnauthorizedException("The UserDetails or appId can not be null");
        }
        log.debug("[getAppAuthorities]::userDetails = [{}]", userDetails);
        return getAppAuthorities(userDetails.getAppId());
    }

    @Override
    public void putAppAuthorities(String appId, Map<String, List<String>> authorities) {
        if (StrUtils.isBlank(appId)) {
            throw new IllegalArgumentException(" The AppId can not be null ");
        }
        log.debug("[putAppAuthorities]::appId = [{}], authorities = [{}]", appId, authorities);
        if (null == authorities || authorities.isEmpty()) {
            log.warn("The authorities of appId:[{}] is empty and will not be cached", appId);
        } else {
            appCache.put(generateAppCacheKey(appId), authorities);
        }
    }

    @Override
    public void removeAppAuthorities(String appId) {
        log.debug("[removeAppAuthorities]::appId = [{}]", appId);
        appCache.remove(generateAppCacheKey(appId));
    }

    @Override
    public void removeAppAuthorities(UserDetails userDetails) {
        log.debug("[removeAppAuthorities]::userDetails = [{}]", userDetails);
        if (null != userDetails) {
            removeAppAuthorities(userDetails.getAppId());
        }
    }

    @Override
    public void removeAllAppAuthorities() {
        log.debug("[removeAllAppAuthorities]::");
        appCache.clear();
    }

    /**
     * 生成用户缓存 Key
     *
     * @param id the id
     * @return the string
     */
    private String generateUserCacheKey(String id) {
        log.debug("[generateUserCacheKey]::id = [{}]", id);
        return generateCacheKey(ConfigManager.getConfig().getAuthority().getUserCacheKeyPrefix(), id);
    }

    /**
     * 生成应用系统缓存 key
     *
     * @param id the id
     * @return the string
     */
    private String generateAppCacheKey(String id) {
        log.debug("[generateAppCacheKey]::id = [{}]", id);
        return generateCacheKey(ConfigManager.getConfig().getAuthority().getUserCacheKeyPrefix(), id);
    }

    /**
     * 构造包含前缀和 唯一凭据 的缓存 key
     *
     * @param orgPrefix 设置的前缀
     * @param id        缓存的唯一 ID
     * @return 最终缓存 key
     */
    private String generateCacheKey(String orgPrefix, String id) {
        if (StrUtils.isBlank(id)) {
            throw new IllegalArgumentException("The id can not be null");
        }
        if (StrUtils.isBlank(orgPrefix)) {
            throw new IllegalArgumentException("The prefix can not be null");
        }
        log.debug("[generateCacheKey]::orgPrefix = [{}], id = [{}]", orgPrefix, id);
        String prefix = orgPrefix.trim();
        prefix = prefix.startsWith(StrUtils.COLON) ? prefix.replaceFirst(StrUtils.COLON, StrUtils.EMPTY_STRING) : prefix;
        return prefix.endsWith(StrUtils.COLON) ? prefix + id : prefix + StrUtils.COLON + id;
    }
}
