/*
 *
 *  *
 *  *   Copyright 2020-2021 Luter.me
 *  *
 *  *   Licensed under the Apache License, Version 2.0 (the "License");
 *  *   you may not use this file except in compliance with the License.
 *  *   You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *   Unless required by applicable law or agreed to in writing, software
 *  *   distributed under the License is distributed on an "AS IS" BASIS,
 *  *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *   See the License for the specific language governing permissions and
 *  *   limitations under the License.
 *  *
 *
 */

package com.luter.heimdall.core.cache;
 
import com.luter.heimdall.core.exception.HeimdallCacheException;
import com.luter.heimdall.core.exception.HeimdallException;
import lombok.Data;
import lombok.experimental.Accessors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

/**
 * 基于 Map 的 内存 缓存实现
 * <p>
 * 不支持过期自动删除，就是普通 map
 *
 * @param <K> the type parameter
 * @param <V> the type parameter
 * @author luter
 */
@Data
@Accessors(chain = true, fluent = true)
public class MapMemoryCache<K, V> implements MemoryCache<K, V> {
    /**
     * The constant log.
     */
    private static final transient Logger log = LoggerFactory.getLogger(MapMemoryCache.class);
    /**
     * The Map.缓存 Map
     */
    private final Map<K, V> map;
    /**
     * 缓存的名字，暂时用作显示，无其他意义
     */
    private String name;

    /**
     * 初始化
     *
     * @param cacheMap the cache map
     * @param name     the name 缓存的名字，用作日志显示
     */
    public MapMemoryCache(Map<K, V> cacheMap, String name) {
        if (cacheMap == null) {
            throw new HeimdallException("cache map cannot be null.name:" + name);
        }
        this.map = cacheMap;
        this.name = name;
        log.trace("Init Map Cache,name = [{}]", name);
    }

    @Override
    public V get(K key) throws HeimdallCacheException {
        final V v = map.get(key);
        log.trace("[{}]=get data from Map Cache ,key:[{}],value:[{}]", name, key, v);
        return v;
    }

    @Override
    public V put(K key, V value) throws HeimdallCacheException {
        map.put(key, value);
        log.trace("[{}]=put data into Map Cache ,key:[{}],value:[{}]", name, key, value);
        return value;
    }

    @Override
    public V remove(K key) throws HeimdallCacheException {
        final V remove = map.remove(key);
        log.trace("[{}]=delete data from Map Cache ,key:[{}],value:[{}]", name, key, remove);
        return remove;
    }

    @Override
    public void clear() throws HeimdallCacheException {
        log.trace("[{}]=clear all data of  Map Cache", name);
        map.clear();
    }

    @Override
    public int size() {
        final int size = map.size();
        log.trace("[{}]=get Map Cache Size :[{}]", name, size);
        return size;
    }

    @Override
    public Set<K> keys() {
        Set<K> keys = map.keySet();
        if (!keys.isEmpty()) {
            final Set<K> ks = Collections.unmodifiableSet(keys);
            log.debug("[{}]=get all  Cached Keys,count:[{}]", name, ks.size());
            return ks;
        }
        log.trace("[{}]=get all  Cached Keys,count:[0}]", name);
        return Collections.emptySet();
    }

    @Override
    public Collection<V> values() {
        Collection<V> values = map.values();
        if (!values.isEmpty()) {
            final Collection<V> vs = Collections.unmodifiableCollection(values);
            log.trace("[{}]=get all Cached  Values,Count:[{}]", name, vs.size());
            return vs;
        }
        log.trace("[{}]=get all Cached  Values,Count:[0]", name);
        return Collections.emptyList();
    }
}
