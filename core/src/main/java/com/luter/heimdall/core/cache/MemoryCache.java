/*
 *
 *  *
 *  *   Copyright 2020-2021 Luter.me
 *  *
 *  *   Licensed under the Apache License, Version 2.0 (the "License");
 *  *   you may not use this file except in compliance with the License.
 *  *   You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *   Unless required by applicable law or agreed to in writing, software
 *  *   distributed under the License is distributed on an "AS IS" BASIS,
 *  *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *   See the License for the specific language governing permissions and
 *  *   limitations under the License.
 *  *
 *
 */

package com.luter.heimdall.core.cache;

import com.luter.heimdall.core.exception.HeimdallCacheException;

import java.util.Collection;
import java.util.Set;

/**
 * 基于内存的缓存定义
 * <p>
 * 主要用在使用内存缓存的情况下，比如:Map、Caffeine 等，用作缓存功能定义。
 * <p>
 * 如果有非缓存的独立存储，
 * <p>
 * 比如: NoSQL 存储 redis、 关系型数据库数据库MySQL
 * <p>
 * 直接实现 对应  token store 和 authority store 即可.
 * <p>
 * 无需实现本接口
 *
 * @param <K> the type parameter:key
 * @param <V> the type parameter:value
 * @author luter
 */
public interface MemoryCache<K, V> {
    /**
     * 获取
     *
     * @param key the key
     * @return the v
     * @throws HeimdallCacheException the auth way cache exception
     */
    V get(K key) throws HeimdallCacheException;

    /**
     * 存入
     *
     * @param key   the key
     * @param value the value
     * @return the v
     * @throws HeimdallCacheException the auth way cache exception
     */
    V put(K key, V value) throws HeimdallCacheException;

    /**
     * 删除
     *
     * @param key the key
     * @return the v
     * @throws HeimdallCacheException the auth way cache exception
     */
    V remove(K key) throws HeimdallCacheException;

    /**
     * 清空
     *
     * @throws HeimdallCacheException the auth way cache exception
     */
    void clear() throws HeimdallCacheException;

    /**
     * 获取总数
     *
     * @return the int
     */
    int size();

    /**
     * 获取所有 keys
     *
     * @return the set
     */
    Set<K> keys();

    /**
     * 获取所有 values();
     *
     * @return the collection
     */
    Collection<V> values();
}
