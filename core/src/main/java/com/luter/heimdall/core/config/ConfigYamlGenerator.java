/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.core.config;

import org.yaml.snakeyaml.LoaderOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.representer.Representer;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class ConfigYamlGenerator {
    public static void main(String[] args) throws IOException {
        generateConfig();
    }

    private static void generateConfig() throws IOException {
        HeimdallProperties heimdallProperties = new HeimdallProperties();
        Yaml yaml = createYaml();
        Writer writer = new FileWriter("config.yaml");
        yaml.dump(heimdallProperties, writer);
    }

    private static Yaml createYaml() {
        LoaderOptions loaderOptions = new LoaderOptions();
        //不允许 Key 重复
        loaderOptions.setAllowDuplicateKeys(false);
        Representer representer = new Representer();
        //跳过不存在的属性，数据套几层都行
        representer.getPropertyUtils().setSkipMissingProperties(true);
        return new Yaml(loaderOptions);
    }
}
