/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.core.config.options;

/**
 * 如何从 request 中获取 token
 * <p>
 * 1、在 Session 模式下，也就是后端服务生成 SessionID 给前端，并且记录登录用户状态的情况下，
 * 应该优先选择使用 Cookie 保存 TokenId,并开启 httpOnly，这样相对安全一些。这种模式下，不建议开启:QUERY方式。
 * <p>
 * 2、如果作为分布式 Token Id 使用，或者客户端不具备 Cookie 条件(小程序、app 等)，此时，Token 信息依然在后台存储
 * 产生的 TokenId 发给客户端，客户端推荐优先使用 Header方式接受参数。
 * 客户端可将 TokenID 设置时间后存于 LocalStorage 或者 SessionStorage里。
 * <p>
 * 3、### 应该尽量使用 Cookie 和 Header 传递 Token 参数
 *
 * @author luter
 */
public enum ResolveTokenOptions {
    /**
     * 从 Cookie 中获取
     */
    COOKIE,
    /**
     * 从 Header 中获取
     */
    HEADER,
    /**
     * 从 Query 中获取,
     */
    QUERY,
    /**
     * 从 header-->query 依次获取
     */
    HEADER_OR_QUERY,
    /**
     * 按照 Cookie->Header->Query 的顺序依次获取，返回第一个获取到的
     */
    ALL


}
