/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.core.config.options;

/**
 * Cookie SameSite 枚举
 * <p>
 * Cookie 的SameSite属性用来限制第三方 Cookie，从而减少安全风险。
 *
 * @author luter
 */
public enum SameSiteOptions {
    /**
     * 无规则
     * Chrome 计划将Lax变为默认设置。这时，网站可以选择显式关闭SameSite属性，将其设为None。
     * <p>
     * 不过，前提是必须同时设置Secure属性（Cookie 只能通过 HTTPS 协议发送），否则无效。
     */
    NONE,
    /**
     * 宽松规则。大多数情况也是不发送第三方 Cookie，但是导航到目标网址的 Get 请求除外。
     */
    LAX,
    /**
     * 最严格规则。完全禁止第三方 Cookie，跨站点时，任何情况下都不会发送 Cookie。
     * <p>
     * 换言之，只有当前网页的 URL 与请求目标一致，才会带上 Cookie。
     */
    STRICT,
}
