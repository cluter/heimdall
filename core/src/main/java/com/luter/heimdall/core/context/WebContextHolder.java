/*
 *
 *  *    Copyright 2020-2021 luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.core.context;

/**
 * 全局 Request 和 Response 功能集合 上下文持有
 *
 * @author luter
 */
public interface WebContextHolder {

    /**
     * Gets request.获取 request 持有对象
     *
     * @return the request
     */
    WebRequestHolder getRequest();

    /**
     * Gets response.获取 response 持有对象
     *
     * @return the response
     */
    WebResponseHolder getResponse();

}
