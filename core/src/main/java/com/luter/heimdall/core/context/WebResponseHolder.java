/*
 *
 *  *    Copyright 2020-2021 luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.core.context;

import com.luter.heimdall.core.cookie.SimpleCookie;

/**
 * response 功能定义
 *
 * @author luter
 */
public interface WebResponseHolder {
    /**
     * 写入 cookie 到 浏览器
     *
     * @param cookie the cookie
     * @return the web response holder
     */
    WebResponseHolder addCookie(SimpleCookie cookie);

    /**
     * 设置返回状态码
     *
     * @param statusCode the status code
     * @return the status
     */
    WebResponseHolder setStatus(int statusCode);

    /**
     * 设置请求头
     *
     * @param name  the name
     * @param value the value
     * @return the header
     */
    WebResponseHolder setHeader(String name, String value);
}
