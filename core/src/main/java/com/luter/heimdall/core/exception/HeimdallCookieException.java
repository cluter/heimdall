/*
 *
 *  *
 *  *   Copyright 2020-2021 Luter.me
 *  *
 *  *   Licensed under the Apache License, Version 2.0 (the "License");
 *  *   you may not use this file except in compliance with the License.
 *  *   You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *   Unless required by applicable law or agreed to in writing, software
 *  *   distributed under the License is distributed on an "AS IS" BASIS,
 *  *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *   See the License for the specific language governing permissions and
 *  *   limitations under the License.
 *  *
 *
 */

package com.luter.heimdall.core.exception;


/**
 * The type Auth way cookie exception.
 *
 * @author luter
 */
public class HeimdallCookieException extends HeimdallException {
    /**
     * The constant serialVersionUID.
     */

    private static final long serialVersionUID = 7030252136847396304L;
    public final static String DEFAULT_MESSAGE = "Cookie Error";

    /**
     * Instantiates a new Auth way cookie exception.
     */
    public HeimdallCookieException() {
        super(DEFAULT_MESSAGE);
    }

    /**
     * Instantiates a new Auth way cookie exception.
     *
     * @param message the message
     */
    public HeimdallCookieException(String message) {
        super(message);
    }

    /**
     * Instantiates a new Auth way cookie exception.
     *
     * @param cause the cause
     */
    public HeimdallCookieException(Throwable cause) {
        super(cause);
    }

    /**
     * Instantiates a new Auth way cookie exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public HeimdallCookieException(String message, Throwable cause) {
        super(message, cause);
    }
}
