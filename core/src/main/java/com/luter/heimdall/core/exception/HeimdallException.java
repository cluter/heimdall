/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.luter.heimdall.core.exception;

/**
 * 基础异常类
 *
 * @author luter
 */
public class HeimdallException extends RuntimeException {

    /**
     * The constant serialVersionUID.
     */

    private static final long serialVersionUID = 1073257005190927785L;
    public final static String DEFAULT_MESSAGE = "An error occurred during processing. Please check the log for more information";

    /**
     * Instantiates a new Auth way exception.
     */
    public HeimdallException() {
        super("[Heimdall] :  "+DEFAULT_MESSAGE);
    }

    /**
     * Instantiates a new Auth way exception.
     *
     * @param message the message
     */
    public HeimdallException(String message) {
        super("[Heimdall] :  "+message);
    }

    /**
     * Instantiates a new Auth way exception.
     *
     * @param cause the cause
     */
    public HeimdallException(Throwable cause) {
        super(cause);
    }

    /**
     * Instantiates a new Auth way exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public HeimdallException(String message, Throwable cause) {
        super("[Heimdall] :  "+message, cause);
    }

}
