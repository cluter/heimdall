/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.core.exception;

/**
 * 登录重试次数超限异常
 *
 * @author Luter
 */
public class HeimdallExcessiveAttemptsException extends HeimdallException {

    private static final long serialVersionUID = -7827912661347922907L;
    public final static String DEFAULT_MESSAGE = "Too many incorrect authentication attempts, your account will be disabled for a while.";

    /**
     * 登录重试次数超限异常
     */
    public HeimdallExcessiveAttemptsException() {
        super(DEFAULT_MESSAGE);
    }

    /**
     * Instantiates a new Account exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public HeimdallExcessiveAttemptsException(String message, Throwable cause) {
        super(message, cause);
    }


    /**
     * Instantiates a new Account exception.
     *
     * @param message the message
     */
    public HeimdallExcessiveAttemptsException(String message) {
        super(message);
    }


}
