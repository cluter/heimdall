/*
 *
 *  *
 *  *      Copyright 2020-2021 Luter.me
 *  *
 *  *      Licensed under the Apache License, Version 2.0 (the "License");
 *  *      you may not use this file except in compliance with the License.
 *  *      You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *      Unless required by applicable law or agreed to in writing, software
 *  *      distributed under the License is distributed on an "AS IS" BASIS,
 *  *      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *      See the License for the specific language governing permissions and
 *  *      limitations under the License.
 *  *
 *
 */

package com.luter.heimdall.core.exception;

/**
 * The type Auth way expired token exception.
 *
 * @author luter
 */
public class HeimdallExpiredTokenException extends HeimdallTokenException {
    /**
     * The constant serialVersionUID.
     */

    private static final long serialVersionUID = 7298585327401903604L;
    public final static String DEFAULT_MESSAGE ="Token is expired";
    /**
     * Instantiates a new Auth way expired token exception.
     */
    public HeimdallExpiredTokenException() {
        super(DEFAULT_MESSAGE);
    }

    /**
     * Instantiates a new Auth way expired token exception.
     *
     * @param message the message
     */
    public HeimdallExpiredTokenException(String message) {
        super(message);
    }

    /**
     * Instantiates a new Auth way expired token exception.
     *
     * @param cause the cause
     */
    public HeimdallExpiredTokenException(Throwable cause) {
        super(cause);
    }

    /**
     * Instantiates a new Auth way expired token exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public HeimdallExpiredTokenException(String message, Throwable cause) {
        super(message, cause);
    }
}