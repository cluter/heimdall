/*
 *
 *  *
 *  *      Copyright 2020-2021 Luter.me
 *  *
 *  *      Licensed under the Apache License, Version 2.0 (the "License");
 *  *      you may not use this file except in compliance with the License.
 *  *      You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *      Unless required by applicable law or agreed to in writing, software
 *  *      distributed under the License is distributed on an "AS IS" BASIS,
 *  *      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *      See the License for the specific language governing permissions and
 *  *      limitations under the License.
 *  *
 *
 */

package com.luter.heimdall.core.exception;

/**
 * Token 错误
 * <p>
 * 从请求中未解析出 token
 * <p>
 * 从缓存中获取 token 出现异常等
 *
 * @author luter
 */
public class HeimdallInvalidTokenException extends HeimdallTokenException {
    /**
     * The constant serialVersionUID.
     */
    private static final long serialVersionUID = 2211353876595199838L;
    public final static String DEFAULT_MESSAGE = "Invalid Token";

    /**
     * Instantiates a new Auth way invalid token exception.
     */
    public HeimdallInvalidTokenException() {
        super(DEFAULT_MESSAGE);
    }

    /**
     * Instantiates a new Auth way invalid token exception.
     *
     * @param message the message
     */
    public HeimdallInvalidTokenException(String message) {
        super(message);
    }

    /**
     * Instantiates a new Auth way invalid token exception.
     *
     * @param cause the cause
     */
    public HeimdallInvalidTokenException(Throwable cause) {
        super(cause);
    }

    /**
     * Instantiates a new Auth way invalid token exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public HeimdallInvalidTokenException(String message, Throwable cause) {
        super(message, cause);
    }
}