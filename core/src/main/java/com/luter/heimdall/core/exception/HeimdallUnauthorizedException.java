/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.core.exception;


/**
 * 未授权
 *
 * @author :luter
 */
public class HeimdallUnauthorizedException extends HeimdallException {

    /**
     * The constant serialVersionUID.
     */

    private static final long serialVersionUID = -6137337625342366734L;
    public final static String DEFAULT_MESSAGE = "You are not authorized to access the resource. Access is denied.";

    /**
     * 未授权异常
     * <p>
     * error: The current User is not authorized.  Access denied.
     */
    public HeimdallUnauthorizedException() {
        super(DEFAULT_MESSAGE);
    }

    /**
     * Instantiates a new Un authenticated exception.
     *
     * @param message the message
     */
    public HeimdallUnauthorizedException(String message) {
        super(message);
    }

    /**
     * Instantiates a new Un authenticated exception.
     *
     * @param cause the cause
     */
    public HeimdallUnauthorizedException(Throwable cause) {
        super(cause);
    }

    /**
     * Instantiates a new Un authenticated exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public HeimdallUnauthorizedException(String message, Throwable cause) {
        super(message, cause);
    }

}
