/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.core.jwt;

import com.luter.heimdall.core.details.UserDetails;
import com.luter.heimdall.core.token.SimpleToken;
import com.luter.heimdall.core.token.id.IdGenerator;

/**
 * Jwt token 生成和校验服务
 *
 * @author luter
 */
public interface JwtProcessor {
    /**
     * 产生 jwt token 串
     *
     * @param userDetails the user details
     * @return the string
     */
    String generate(UserDetails userDetails);

    /**
     * 产生 jwt token 串
     *
     * @param payloadStr SimpleToken 转字符串
     * @return the string
     */
    String generate(String payloadStr);

    /**
     * 产生 jwt token 串
     *
     * @param token the token
     * @return the string
     */
    String generate(SimpleToken token);

    /**
     * 校验 jwt token 合法性
     *
     * @param token the token
     * @return the simple token
     */
    SimpleToken verify(String token);

    /**
     * 生成 非对称加密的 公钥
     *
     * @return the object
     */
    default Object generatePublicKey() {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * 拿到 ID 生成器
     *
     * @return the id generator
     */
    IdGenerator getIdGenerator();

    /**
     * 设置 ID 生成器
     *
     * @param idGenerator the id generator
     * @return the id generator
     */
    JwtProcessor setIdGenerator(IdGenerator idGenerator);

}
