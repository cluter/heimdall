/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.core.limiter;


/**
 * 登录密码重试次数限制
 * <p>
 * eg:以登录流程为例
 * <p>
 * 1、在登录方法中校验密码出现错误，执行 overLimitWhenIncremented(key) 记录错误次数.
 * <p>
 * 其中：key 用来唯一代表当前登录的这个用户，可以是用户 ID 或者手机号等唯一字段，这个值被用作缓存 key.
 * <p>
 * 2、如果 overLimitWhenIncremented(key) 返回 true，则说明超过限制了，可以直接抛出重试超限异常，或者更新用户状态等操作。
 * <p>
 * 如果 overLimitWhenIncremented 返回 false,则提示密码错误。可通过 availableTimes() 获取剩余可重试次数
 *
 * <p>
 * 3、如果在规定次数内输入正确，执行 release(key)方法，释放锁定状态。继续执行后续登录逻辑。
 *
 * @author Luter
 */
public interface PasswordRetryLimiter {

    /**
     * 增加缓存重试次数
     * <p>
     * 每重试一次，调用一次，缓存的次数递增+1
     *
     * @param key 根据业务提供一个唯一的 Key 用来标识限制谁的登录次数，可以是手机号，账号等能唯一标识是哪个用户在尝试登录
     * @return 超过次数限制返回 true,否则返回 false
     */
    boolean overLimitWhenIncremented(String key);

    /**
     * 释放锁定状态
     *
     * @param key the key
     */
    void release(String key);

    /**
     * 已经重试的次数
     *
     * @param key the key
     * @return the int
     */
    int count(String key);

    /**
     * 剩余有效次数，也就是说还可以重试几次
     *
     * @param key the key
     * @return the int
     */
    int availableTimes(String key);


}
