/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.core.listener;


import com.luter.heimdall.core.details.UserDetails;
import com.luter.heimdall.core.token.SimpleToken;

import java.util.Collection;

/**
 * 认证授权事件监听
 *
 * @author luter
 */
public interface AuthEventListener {
    /**
     * 监听器的名字
     * <p>
     * 用作显示和区分，默认为空
     *
     * @return the string
     */
    default String name() {
        return "";
    }

    /**
     * 登录事件
     *
     * @param token 登录成功后发出的 token
     */
    default void onLogin(SimpleToken token) {
    }

    /**
     * 注销事件
     *
     * @param token 被注销的 token
     */
    default void onLogout(SimpleToken token) {
    }

    /**
     * 删除 token 事件，也就是会话结束
     * <p>
     * 只有在成功删除后触发
     *
     * @param token 被删除的 token
     */
    default void onTokenKick(SimpleToken token) {
    }

    /**
     * 删除 principal tokens 事件，也就是会话结束
     *
     * @param userDetails 用户详情
     * @param count       被踢出 token 数量
     */
    default void onPrincipalKick(UserDetails userDetails, int count) {
    }

    /**
     * 资源授权结果事件
     *
     * @param result      授权结果,true通过还是false拒绝
     * @param userDetails 用户详情
     * @param method      请求方法
     * @param url         请求 url
     * @param isThrowEx   授权不通过是否抛出异常
     * @see ResourceAuthorizedResultType
     */
    default void onResourceAuthorized(ResourceAuthorizedResultType result, UserDetails userDetails, String method, String url, boolean isThrowEx) {

    }

    /**
     * 获取当前所有注册的 认证授权 监听器
     *
     * @return the listeners
     */
    Collection<AuthEventListener> getListeners();

    /**
     * 资源授权事件类型
     *
     * @author luter
     */
    enum ResourceAuthorizedResultType {
        /**
         * 应用权限为空，默认放行
         */
        APP_RULE_EMPTY,
        /**
         * 用户权限为空，默认拒绝
         */
        USER_RULE_EMPTY,
        /**
         * 不具备权限，资源访问被拒绝
         */
        DENIED,
        /**
         * 具备权限，资源访问通过
         */
        ACCEPTED;
    }
}
