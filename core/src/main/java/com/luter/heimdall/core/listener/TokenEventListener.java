/*
 *
 *  *
 *  *      Copyright 2020-2021 Luter.me
 *  *
 *  *      Licensed under the Apache License, Version 2.0 (the "License");
 *  *      you may not use this file except in compliance with the License.
 *  *      You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *      Unless required by applicable law or agreed to in writing, software
 *  *      distributed under the License is distributed on an "AS IS" BASIS,
 *  *      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *      See the License for the specific language governing permissions and
 *  *      limitations under the License.
 *  *
 *
 */

package com.luter.heimdall.core.listener;

import com.luter.heimdall.core.token.SimpleToken;

import java.util.Collection;

/**
 * Token 事件监听
 *
 * @author luter
 */
public interface TokenEventListener {

    /**
     * 监听器的名字
     * <p>
     * 监听器的唯一标识，不能为空
     *
     * @return the string
     */
    default String name() {
        return "";
    }

    /**
     * 创建
     *
     * @param token 新创建的 token
     */
    default void onCreated(SimpleToken token) {
    }

    /**
     * 读取
     *
     * @param token 读取到的 token
     */
    default void onRead(SimpleToken token) {
    }

    /**
     * 修改
     *
     * @param token 修改成功后的 token
     */
    default void onUpdated(SimpleToken token) {
    }

    /**
     * 删除
     *
     * @param token 被删除的 token
     */
    default void onDeleted(SimpleToken token) {
    }

    /**
     * Gets listeners.
     *
     * @return the listeners
     */
    Collection<TokenEventListener> getListeners();

}
