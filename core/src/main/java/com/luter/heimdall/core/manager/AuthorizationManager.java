/*
 *
 *  *    Copyright 2020-2021 luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.core.manager;

import com.luter.heimdall.core.authorization.provider.AuthorityDataProvider;
import com.luter.heimdall.core.authorization.store.AuthorizationStore;
import com.luter.heimdall.core.details.UserDetails;
import com.luter.heimdall.core.listener.AuthEventListener;
import com.luter.heimdall.core.utils.HttpMethodEnum;
import com.luter.heimdall.core.utils.PathUtil;

import java.util.Collection;

/**
 * 请求资源 授权管理器
 * <p>
 * 实现：
 * <p>
 * 匹配请求资源的 method 和 url
 * <p>
 * 1、首先对是否通过认证(已登录)进行判断
 * <p>
 * 2、根据应用权限与用户权限进行比对，确定是否授权。
 * <p>
 * 使用：
 * <p>
 * 通过 Filter 或者 Interceptor 对请求进行拦截，然后处理判断。
 *
 * @author luter
 */
public interface AuthorizationManager {
    /**
     * 匹配所有请求方式，如 GET POST PUT等等
     */
    String ALL_METHOD_NAME = "ALL";
    ////////////////////////路由 授权//////////////////////////////

    /**
     * 判断当前登录用户对指定资源是否具有权限
     *
     * @param method    请求方法
     * @param url       请求 url
     * @param isThrowEx 如果失败， 是否抛出异常?
     * @return the boolean 是否授权通过
     */
    boolean isAuthorized(String method, String url, boolean isThrowEx);

    /**
     * 判断当前登录用户对指定资源是否具有权限
     *
     * @param userDetails 用户详情,用户必须登录
     * @param method      请求方法
     * @param url         请求 url
     * @param isThrowEx   是否抛出异常
     * @return the boolean
     * @see HttpMethodEnum#isValidMethod(String)
     * @see PathUtil#isValidPath(String)
     * @see AuthorizationStore
     * @see AuthorityDataProvider
     */
    boolean isAuthorized(UserDetails userDetails, String method, String url, boolean isThrowEx);

    /**
     * 获取 权限 Store
     *
     * @return the authorization store
     */
    AuthorizationStore getAuthorizationStore();

    /**
     * 获取认证管理器
     *
     * @return the authorization manager
     */
    AuthenticationManager getAuthenticationManager();

    //////// //////// //////// 监听器  //////// //////// ////////

    /**
     * 获取当前所有注册的 认证授权 监听器
     *
     * @return the listeners
     */
    Collection<AuthEventListener> getListeners();
}
