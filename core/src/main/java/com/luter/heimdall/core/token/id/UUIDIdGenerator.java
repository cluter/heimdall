/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.core.token.id;

import org.slf4j.Logger;

import java.util.UUID;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Token ID 生成 UUID实现，去掉了横线
 *
 * @author luter
 */
public class UUIDIdGenerator implements IdGenerator {
    private static final transient Logger log = getLogger(UUIDIdGenerator.class);

    @Override
    public String generate() {
        final String id = UUID.randomUUID().toString().replace("-", "");
        log.debug("[generate]::id = [{}]", id);
        return id;
    }
}
