/*
 *
 *  *    Copyright 2020-2021 luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.core.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * HttpMethod 枚举
 *
 * @author luter
 */
public enum HttpMethodEnum {
    /**
     * Get http method enum.
     */
    GET,
    /**
     * Head http method enum.
     */
    HEAD,
    /**
     * Post http method enum.
     */
    POST,
    /**
     * Put http method enum.
     */
    PUT,
    /**
     * Patch http method enum.
     */
    PATCH,
    /**
     * Delete http method enum.
     */
    DELETE,
    /**
     * Options http method enum.
     */
    OPTIONS,
    /**
     * Trace http method enum.
     */
    TRACE;

    /**
     * The constant mappings.
     */
    private static final Map<String, HttpMethodEnum> MAPPINGS = new HashMap<>(16);

    /**
     * Instantiates a new Http method enum.
     */
    HttpMethodEnum() {
    }

    /**
     * Resolve http method enum.
     *
     * @param method the method
     * @return the http method enum
     */
    public static HttpMethodEnum resolve(String method) {
        return method != null ? MAPPINGS.get(method) : null;
    }

    /**
     * 是不是一个合法的 HttpMethod
     *
     * @param method the method
     * @return the boolean
     */
    public static boolean isValidMethod(String method) {
        return null != method && method.length() > 0 && null != resolve(method.toUpperCase());

    }

    /**
     * Matches boolean.
     *
     * @param method the method
     * @return the boolean
     */
    public boolean matches(String method) {
        return this.name().equals(method);
    }

    static {
        HttpMethodEnum[] methodEnums = values();
        for (HttpMethodEnum httpMethod : methodEnums) {
            MAPPINGS.put(httpMethod.name(), httpMethod);
        }

    }
}
