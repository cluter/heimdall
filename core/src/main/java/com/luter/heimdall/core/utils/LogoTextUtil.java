/*
 *
 *  *    Copyright 2020-2021 luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.core.utils;

/**
 * 显示艺术字在日志里
 *
 * @author luter
 */
public abstract class LogoTextUtil {
    private LogoTextUtil() {
    }

    private final static String TEXT1 = "\n" +
            " .----------------.  .----------------.  .----------------.  .----------------.  .----------------.  .----------------.  .----------------. \n" +
            "| .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. |\n" +
            "| |      __      | || | _____  _____ | || |  _________   | || |  ____  ____  | || | _____  _____ | || |      __      | || |  ____  ____  | |\n" +
            "| |     /  \\     | || ||_   _||_   _|| || | |  _   _  |  | || | |_   ||   _| | || ||_   _||_   _|| || |     /  \\     | || | |_  _||_  _| | |\n" +
            "| |    / /\\ \\    | || |  | |    | |  | || | |_/ | | \\_|  | || |   | |__| |   | || |  | | /\\ | |  | || |    / /\\ \\    | || |   \\ \\  / /   | |\n" +
            "| |   / ____ \\   | || |  | '    ' |  | || |     | |      | || |   |  __  |   | || |  | |/  \\| |  | || |   / ____ \\   | || |    \\ \\/ /    | |\n" +
            "| | _/ /    \\ \\_ | || |   \\ `--' /   | || |    _| |_     | || |  _| |  | |_  | || |  |   /\\   |  | || | _/ /    \\ \\_ | || |    _|  |_    | |\n" +
            "| ||____|  |____|| || |    `.__.'    | || |   |_____|    | || | |____||____| | || |  |__/  \\__|  | || ||____|  |____|| || |   |______|   | |\n" +
            "| |              | || |              | || |              | || |              | || |              | || |              | || |              | |\n" +
            "| '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' |\n" +
            " '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------' \n" +
            "         :: Author: Luter ::                                                                         :: Heimdall ::    (%s)                  \n";


    private final static String TEXT2 = "\n" +
            "     ___      __    __  .___________. __    __  ____    __    ____  ___   ____    ____ \n" +
            "    /   \\    |  |  |  | |           ||  |  |  | \\   \\  /  \\  /   / /   \\  \\   \\  /   / \n" +
            "   /  ^  \\   |  |  |  | `---|  |----`|  |__|  |  \\   \\/    \\/   / /  ^  \\  \\   \\/   /  \n" +
            "  /  /_\\  \\  |  |  |  |     |  |     |   __   |   \\            / /  /_\\  \\  \\_    _/   \n" +
            " /  _____  \\ |  `--'  |     |  |     |  |  |  |    \\    /\\    / /  _____  \\   |  |     \n" +
            "/__/     \\__\\ \\______/      |__|     |__|  |__|     \\__/  \\__/ /__/     \\__\\  |__|     \n" +
            "                                                                                       \n" +
            "   :: Author: Luter ::                                     :: Heimdall ::    (%s)       \n";


    private final static String TEXT3 = "\n" +
            "     _         _   _  __        __          \n" +
            "    / \\  _   _| |_| |_\\ \\      / /_ _ _   _ \n" +
            "   / _ \\| | | | __| '_ \\ \\ /\\ / / _` | | | |\n" +
            "  / ___ \\ |_| | |_| | | \\ V  V / (_| | |_| |\n" +
            " /_/   \\_\\__,_|\\__|_| |_|\\_/\\_/ \\__,_|\\__, |\n" +
            "                                      |___/ \n" +
            " :: Author: Luter ::  :: Heimdall :: (%s)  \n";

    private final static String TEXT4 = "\n" +
            "   ____     __    __   ________   __    __   ___       ___     ____    __      __ \n" +
            "  (    )    ) )  ( (  (___  ___) (  \\  /  ) (  (       )  )   (    )   ) \\    / ( \n" +
            "  / /\\ \\   ( (    ) )     ) )     \\ (__) /   \\  \\  _  /  /    / /\\ \\    \\ \\  / /  \n" +
            " ( (__) )   ) )  ( (     ( (       ) __ (     \\  \\/ \\/  /    ( (__) )    \\ \\/ /   \n" +
            "  )    (   ( (    ) )     ) )     ( (  ) )     )   _   (      )    (      \\  /    \n" +
            " /  /\\  \\   ) \\__/ (     ( (       ) )( (      \\  ( )  /     /  /\\  \\      )(     \n" +
            "/__(  )__\\  \\______/     /__\\     /_/  \\_\\      \\_/ \\_/     /__(  )__\\    /__\\    \n" +
            "                                                                                  \n" +
            "   :: Author: Luter ::  ::                                 Heimdall :: (%s)        \n";

    private final static String TEXT5 = "\n" +
            "      .o.                       .   oooo        oooooo   oooooo     oooo                       \n" +
            "     .888.                    .o8   `888         `888.    `888.     .8'                        \n" +
            "    .8\"888.     oooo  oooo  .o888oo  888 .oo.     `888.   .8888.   .8'    .oooo.   oooo    ooo \n" +
            "   .8' `888.    `888  `888    888    888P\"Y88b     `888  .8'`888. .8'    `P  )88b   `88.  .8'  \n" +
            "  .88ooo8888.    888   888    888    888   888      `888.8'  `888.8'      .oP\"888    `88..8'   \n" +
            " .8'     `888.   888   888    888 .  888   888       `888'    `888'      d8(  888     `888'    \n" +
            "o88o     o8888o  `V88V\"V8P'   \"888\" o888o o888o       `8'      `8'       `Y888\"\"8o     .8'     \n" +
            "                                                                                   .o..P'      \n" +
            "  :: Author: Luter ::  ::                           Heimdall :: (%s)           `Y8P'            \n" +
            "                                                                                               \n";


    /**
     * 显示艺术字在日志里
     *
     * @param version the version
     */
    public static void showText(int no, String version) {
        String choose;
        switch (no) {
            case 1:
                choose = TEXT1;
                break;
            case 3:
                choose = TEXT3;
                break;
            case 4:
                choose = TEXT4;
                break;
            case 5:
                choose = TEXT5;
                break;
            default:
                choose = TEXT2;
                break;

        }
        final String format = String.format(choose, version);
        System.out.println(format);
    }

    public static void showText(String version) {
        showText(0, version);
    }

    public static void main(String[] args) {
        LogoTextUtil.showText(5, "1.0.1");
    }
}
