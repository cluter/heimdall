/*
 *
 *  *
 *  *   Copyright 2020-2021 Luter.me
 *  *
 *  *   Licensed under the Apache License, Version 2.0 (the "License");
 *  *   you may not use this file except in compliance with the License.
 *  *   You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *   Unless required by applicable law or agreed to in writing, software
 *  *   distributed under the License is distributed on an "AS IS" BASIS,
 *  *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *   See the License for the specific language governing permissions and
 *  *   limitations under the License.
 *  *
 *
 */

package com.luter.heimdall.core.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * The type Obj util.
 *
 * @author luter
 */
public abstract class ObjUtil {
    private ObjUtil() {
    }

    /**
     * Null safe equals boolean.
     *
     * @param o1 the o 1
     * @param o2 the o 2
     * @return the boolean
     */
    public static boolean nullSafeEquals(Object o1, Object o2) {
        if (o1 == o2) {
            return true;
        }
        if (o1 == null || o2 == null) {
            return false;
        }
        if (o1.equals(o2)) {
            return true;
        }
        if (o1.getClass().isArray() && o2.getClass().isArray()) {
            return arrayEquals(o1, o2);
        }
        return false;
    }

    /**
     * Array equals boolean.
     *
     * @param o1 the o 1
     * @param o2 the o 2
     * @return the boolean
     */
    private static boolean arrayEquals(Object o1, Object o2) {
        if (o1 instanceof Object[] && o2 instanceof Object[]) {
            return Arrays.equals((Object[]) o1, (Object[]) o2);
        }
        if (o1 instanceof boolean[] && o2 instanceof boolean[]) {
            return Arrays.equals((boolean[]) o1, (boolean[]) o2);
        }
        if (o1 instanceof byte[] && o2 instanceof byte[]) {
            return Arrays.equals((byte[]) o1, (byte[]) o2);
        }
        if (o1 instanceof char[] && o2 instanceof char[]) {
            return Arrays.equals((char[]) o1, (char[]) o2);
        }
        if (o1 instanceof double[] && o2 instanceof double[]) {
            return Arrays.equals((double[]) o1, (double[]) o2);
        }
        if (o1 instanceof float[] && o2 instanceof float[]) {
            return Arrays.equals((float[]) o1, (float[]) o2);
        }
        if (o1 instanceof int[] && o2 instanceof int[]) {
            return Arrays.equals((int[]) o1, (int[]) o2);
        }
        if (o1 instanceof long[] && o2 instanceof long[]) {
            return Arrays.equals((long[]) o1, (long[]) o2);
        }
        if (o1 instanceof short[] && o2 instanceof short[]) {
            return Arrays.equals((short[]) o1, (short[]) o2);
        }
        return false;
    }

    /**
     * Null safe hash code int.
     *
     * @param obj the obj
     * @return the int
     */
    public static int nullSafeHashCode(Object obj) {
        if (obj == null) {
            return 0;
        }
        if (obj.getClass().isArray()) {
            if (obj instanceof Object[]) {
                return nullSafeHashCode(obj);
            }
            if (obj instanceof boolean[]) {
                return nullSafeHashCode(obj);
            }
            if (obj instanceof byte[]) {
                return nullSafeHashCode(obj);
            }
            if (obj instanceof char[]) {
                return nullSafeHashCode(obj);
            }
            if (obj instanceof double[]) {
                return nullSafeHashCode(obj);
            }
            if (obj instanceof float[]) {
                return nullSafeHashCode(obj);
            }
            if (obj instanceof int[]) {
                return nullSafeHashCode(obj);
            }
            if (obj instanceof long[]) {
                return nullSafeHashCode(obj);
            }
            if (obj instanceof short[]) {
                return nullSafeHashCode(obj);
            }
        }
        return obj.hashCode();
    }

    /**
     * Convert object to map map.
     *
     * @param obj          the obj
     * @param ignoredProps the ignored props
     * @return the map
     */
    public static Map<String, Object> ConvertObjectToMap(Object obj, String... ignoredProps) {
        try {
            Method[] methods = obj.getClass().getMethods();
            Map<String, Object> map = new HashMap<>();
            for (Method m : methods) {
                if (m.getName().startsWith("get") && !m.getName().startsWith("getClass")) {
                    Object value = m.invoke(obj);
                    final String prop = m.getName().substring(3).toLowerCase();
                    if (null != ignoredProps && Arrays.asList(ignoredProps).contains(prop)) {
                    } else {
                        map.put(prop, value);
                    }

                }
            }
            return map;
        } catch (InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return new HashMap<>();
    }
}
