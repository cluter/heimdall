/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.plugins.aspectj;

import com.luter.heimdall.core.authorization.handler.HeimdallMethodAuthorizationHandler;
import com.luter.heimdall.core.exception.HeimdallUnauthorizedException;
import com.luter.heimdall.core.manager.AuthorizationManager;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.reflect.Method;

/**
 * 权限注解拦截器,基于 AspectJ 实现
 * <p>
 * 1、首先匹配方法注解，进入权限验证环节
 * <p>
 * 2、拿到注解上需要的角色或者权限标识符
 * <p>
 * 3、拿到用户自身信息中具备的所有角色或者权限标识符
 * <p>
 * 4、遍历所有用户角色或者权限，如果角色或者权限标识匹配，则授权通过，否则不通过
 *
 * @author Luter
 */
@Aspect
public class HeimdallAspectAnnotationHandler {
    /**
     * 切入点
     */
    private static final String POINT_CUP_EXPRESSION =
            "@within(com.luter.heimdall.core.annotation.RequiresUser)||@annotation(com.luter.heimdall.core.annotation.RequiresUser)" +
                    "||@within(com.luter.heimdall.core.annotation.RequiresRole)||@annotation(com.luter.heimdall.core.annotation.RequiresRole)" +
                    "||@within(com.luter.heimdall.core.annotation.RequiresRoles)||@annotation(com.luter.heimdall.core.annotation.RequiresRoles)" +
                    "||@within(com.luter.heimdall.core.annotation.RequiresPermission)||@annotation(com.luter.heimdall.core.annotation.RequiresPermission)" +
                    "||@within(com.luter.heimdall.core.annotation.RequiresPermissions)||@annotation(com.luter.heimdall.core.annotation.RequiresPermissions)";

    private final AuthorizationManager authorizationManager;

    public HeimdallAspectAnnotationHandler(AuthorizationManager authorizationManager) {
        this.authorizationManager = authorizationManager;
    }

    /**
     * 切入点
     */
    @Pointcut(POINT_CUP_EXPRESSION)
    public void pointCut() {
    }

    /**
     * 执行前校验
     *
     * @param joinPoint the join point
     */
    @Before("pointCut()")
    public void onBefore(JoinPoint joinPoint) throws HeimdallUnauthorizedException {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        final Method method = signature.getMethod();
        HeimdallMethodAuthorizationHandler.handleMethodAnnotation(authorizationManager, method);
    }

}
