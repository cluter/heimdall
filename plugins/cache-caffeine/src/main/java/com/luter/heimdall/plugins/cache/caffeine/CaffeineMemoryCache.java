/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.plugins.cache.caffeine;

import com.github.benmanes.caffeine.cache.Cache;
import com.luter.heimdall.core.cache.MemoryCache;
import com.luter.heimdall.core.exception.HeimdallCacheException;
import com.luter.heimdall.core.exception.HeimdallException;
import lombok.Data;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.slf4j.Logger;

import java.util.Collection;
import java.util.Set;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * 基于Caffeine 的缓存实现
 * <p>
 * eg:
 * <p>
 * final Cache<String, SimpleToken> caffeineTokenCache = Caffeine.newBuilder().expireAfterAccess(Duration.ofHours(1)).build();
 * <p>
 * SimpleCache<String, SimpleToken> tokenCache = new SimpleCaffeineCache<>(caffeineTokenCache);
 *
 * @param <K> the type parameter
 * @param <V> the type parameter
 * @author Luter.
 */
@Data
@Accessors(chain = true, fluent = true)
public class CaffeineMemoryCache<K, V> implements MemoryCache<K, V> {
    /**
     * The constant log.
     */
    private static final transient Logger log = getLogger(CaffeineMemoryCache.class);

    /**
     * 缓存
     */
    private Cache<K, V> cache;

    /**
     * 名称
     */
    private String name;

    /**
     * 基于Caffeine 的缓存实现
     * <p>
     * eg:
     * <p>
     * final Cache<String, SimpleToken> caffeineTokenCache = Caffeine.newBuilder().expireAfterAccess(Duration.ofHours(12)).build();
     * <p>
     * SimpleCache<String, SimpleToken> tokenCache = new SimpleCaffeineCache<>(caffeineTokenCache);
     *
     * @param cache the cache
     * @param name  the name 名字，显示用，无业务意义
     */
    public CaffeineMemoryCache(Cache<K, V> cache, String name) {
        if (cache == null) {
            throw new HeimdallException("Caffeine Cache can not be null.");
        }
        this.cache = cache;
        this.name = name;
        log.trace(" Caffeine Cache initialized,name:[{}] ", name);
    }

    @Override
    public V get(K key) throws HeimdallCacheException {
        final V value = this.cache.getIfPresent(key);
        log.trace("[{}]=get Caffeine data,key=[{}],value=[{}]", name, key, value);
        return value;
    }

    @Override
    public V put(K key, V value) throws HeimdallCacheException {
        log.trace("[{}]=put data to Caffeine ,key=[{}],value=[{}]", name, key, value);
        this.cache.put(key, value);
        return value;
    }

    @Override
    public V remove(K key) throws HeimdallCacheException {
        final V previous = get(key);
        this.cache.invalidate(key);
        log.trace("[{}]=remove data from  Caffeine ,key=[{}],value=[{}]", name, key, previous);
        return previous;
    }

    @Override
    public void clear() throws HeimdallCacheException {
        log.trace("[{}]=clear all data from  Caffeine", name);
        this.cache.invalidateAll();
    }

    @Override
    public int size() {
        final int size = this.cache.asMap().size();
        log.trace("[{}]=get Caffeine cached size,size=[{}]", name, size);
        return size;
    }

    @Override
    public Set<K> keys() {
        final Set<@NonNull K> ks = this.cache.asMap().keySet();
        log.trace("[{}]=get all Caffeine cached keys,count=[{}]", name, ks.size());
        return ks;
    }

    @Override
    public Collection<V> values() {
        final Collection<@NonNull V> values = this.cache.asMap().values();
        log.trace("[{}]=get all cached data from  Caffeine,count=[{}]", name, values.size());
        return values;
    }
}
