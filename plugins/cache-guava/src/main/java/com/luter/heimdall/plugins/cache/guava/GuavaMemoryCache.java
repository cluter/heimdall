/*
 *
 *  *    Copyright 2020-2021 luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.plugins.cache.guava;

import com.google.common.cache.Cache;
import com.luter.heimdall.core.cache.MemoryCache;
import com.luter.heimdall.core.exception.HeimdallCacheException;
import lombok.Data;
import lombok.experimental.Accessors;
import org.slf4j.Logger;

import java.util.Collection;
import java.util.Set;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Guava 内存缓存
 *
 * @param <K> the type parameter
 * @param <V> the type parameter
 * @author luter
 */
@Data
@Accessors(chain = true, fluent = true)
public class GuavaMemoryCache<K, V> implements MemoryCache<K, V> {
    /**
     * The constant log.
     */
    private static final transient Logger log = getLogger(GuavaMemoryCache.class);

    /**
     * 缓存
     */
    private Cache<K, V> cache;

    /**
     * 名字
     */
    private String name;

    /**
     * Instantiates a new Simple guava cache.
     *
     * @param cache the cache
     * @param name  the name
     */
    public GuavaMemoryCache(Cache<K, V> cache, String name) {
        if (cache == null) {
            throw new HeimdallCacheException("Guava Cache can not be null.");
        }
        this.cache = cache;
        this.name = name;
    }

    @Override
    public V get(K key) throws HeimdallCacheException {
        final V v = cache.getIfPresent(key);
        log.trace("[get]::key = [{}],value = [{}]", key, v);
        return v;
    }

    @Override
    public V put(K key, V value) throws HeimdallCacheException {
        cache.put(key, value);
        log.trace("[put]::key = [{}], value = [{}]", key, value);
        return value;
    }

    @Override
    public V remove(K key) throws HeimdallCacheException {
        final V remove = cache.asMap().remove(key);
        log.trace("[remove]::key = [{}],value = [{}]", key, remove);
        return remove;
    }

    @Override
    public void clear() throws HeimdallCacheException {
        log.trace("[clear]:: clear all cache data");
        cache.asMap().clear();
    }

    @Override
    public int size() {
        final int size = cache.asMap().size();
        log.trace("[size]:: size = [{}]", size);
        return size;
    }

    @Override
    public Set<K> keys() {
        final Set<K> ks = cache.asMap().keySet();
        log.trace("[keys]:: keys size = [{}]", ks);
        return ks;
    }

    @Override
    public Collection<V> values() {
        final Collection<V> values = cache.asMap().values();
        log.trace("[values]:: values count = [{}]", values.size());
        return values;
    }
}
