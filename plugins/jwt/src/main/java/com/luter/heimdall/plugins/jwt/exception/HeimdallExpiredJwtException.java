/*
 *
 *  *
 *  *   Copyright 2020-2021 Luter.me
 *  *
 *  *   Licensed under the Apache License, Version 2.0 (the "License");
 *  *   you may not use this file except in compliance with the License.
 *  *   You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *   Unless required by applicable law or agreed to in writing, software
 *  *   distributed under the License is distributed on an "AS IS" BASIS,
 *  *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *   See the License for the specific language governing permissions and
 *  *   limitations under the License.
 *  *
 *
 */

package com.luter.heimdall.plugins.jwt.exception;

/**
 * jwt token 过期,意味着需要重新登录
 *
 * @author luter
 */
public class HeimdallExpiredJwtException extends HeimdallJwtException {
    /**
     * The constant serialVersionUID.
     */

    private static final long serialVersionUID = -205054632492095187L;
    public final static String DEFAULT_MESSAGE = "The token has expired";

    /**
     * Instantiates a new Auth way expired jwt exception.
     */
    public HeimdallExpiredJwtException() {
        super(DEFAULT_MESSAGE);
    }

    /**
     * Instantiates a new Auth way expired jwt exception.
     *
     * @param message the message
     */
    public HeimdallExpiredJwtException(String message) {
        super(message);
    }

    /**
     * Instantiates a new Auth way expired jwt exception.
     *
     * @param cause the cause
     */
    public HeimdallExpiredJwtException(Throwable cause) {
        super(cause);
    }

    /**
     * Instantiates a new Auth way expired jwt exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public HeimdallExpiredJwtException(String message, Throwable cause) {
        super(message, cause);
    }
}