/*
 *
 *  *
 *  *   Copyright 2020-2021 Luter.me
 *  *
 *  *   Licensed under the Apache License, Version 2.0 (the "License");
 *  *   you may not use this file except in compliance with the License.
 *  *   You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *   Unless required by applicable law or agreed to in writing, software
 *  *   distributed under the License is distributed on an "AS IS" BASIS,
 *  *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *   See the License for the specific language governing permissions and
 *  *   limitations under the License.
 *  *
 *
 */

package com.luter.heimdall.plugins.jwt.exception;


/**
 * Token 格式不正确
 *
 * @author luter
 */
public class HeimdallInvalidJwtException extends HeimdallJwtException {
    /**
     * The constant serialVersionUID.
     */

    private static final long serialVersionUID = -6340342712695288855L;
    public final static String DEFAULT_MESSAGE = "Failed to verify token.Invalid token";

    /**
     * Instantiates a new Auth way invalid jwt exception.
     */
    public HeimdallInvalidJwtException() {
        super(DEFAULT_MESSAGE);
    }

    /**
     * Instantiates a new Auth way invalid jwt exception.
     *
     * @param message the message
     */
    public HeimdallInvalidJwtException(String message) {
        super(message);
    }

    /**
     * Instantiates a new Auth way invalid jwt exception.
     *
     * @param cause the cause
     */
    public HeimdallInvalidJwtException(Throwable cause) {
        super(cause);
    }

    /**
     * Instantiates a new Auth way invalid jwt exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public HeimdallInvalidJwtException(String message, Throwable cause) {
        super(message, cause);
    }
}