/*
 *
 *  *
 *  *   Copyright 2020-2021 Luter.me
 *  *
 *  *   Licensed under the Apache License, Version 2.0 (the "License");
 *  *   you may not use this file except in compliance with the License.
 *  *   You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *   Unless required by applicable law or agreed to in writing, software
 *  *   distributed under the License is distributed on an "AS IS" BASIS,
 *  *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *   See the License for the specific language governing permissions and
 *  *   limitations under the License.
 *  *
 *
 */

package com.luter.heimdall.plugins.jwt.exception;

import com.luter.heimdall.core.exception.HeimdallException;

/**
 * jwt 基础异常
 *
 * @author luter
 */
public class HeimdallJwtException extends HeimdallException {
    /**
     * The constant serialVersionUID.
     */

    private static final long serialVersionUID = 3788027825646221311L;
    public final static String DEFAULT_MESSAGE = "Error occurred while processing token";

    /**
     * Instantiates a new Auth way jwt exception.
     */
    public HeimdallJwtException() {
        super(DEFAULT_MESSAGE);
    }

    /**
     * Instantiates a new Auth way jwt exception.
     *
     * @param message the message
     */
    public HeimdallJwtException(String message) {
        super(message);
    }

    /**
     * Instantiates a new Auth way jwt exception.
     *
     * @param cause the cause
     */
    public HeimdallJwtException(Throwable cause) {
        super(cause);
    }

    /**
     * Instantiates a new Auth way jwt exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public HeimdallJwtException(String message, Throwable cause) {
        super(message, cause);
    }
}