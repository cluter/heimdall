/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.plugins.jwt.processor;

 
import com.luter.heimdall.core.config.ConfigManager;
import com.luter.heimdall.core.config.HeimdallProperties;
import com.luter.heimdall.core.details.UserDetails;
import com.luter.heimdall.core.jwt.JwtProcessor;
import com.luter.heimdall.core.token.SimpleToken;
import com.luter.heimdall.core.token.id.IdGenerator;
import com.luter.heimdall.core.token.id.UUIDIdGenerator;
import com.luter.heimdall.core.utils.StrUtils;
import com.luter.heimdall.core.utils.crypto.Md5Util;
import com.luter.heimdall.plugins.jwt.exception.HeimdallExpiredJwtException;
import com.luter.heimdall.plugins.jwt.exception.HeimdallInvalidJwtException;
import com.luter.heimdall.plugins.jwt.exception.HeimdallJwtException;
import com.luter.heimdall.plugins.jwt.util.JacksonUtil;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import org.slf4j.Logger;

import java.text.ParseException;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Nimbus HMAC 算法生成 jwt token 实现
 *
 * @author luter
 */
public class NimbusHMACJwtProcessor implements JwtProcessor {

    /**
     * The constant log.
     */
    private static final transient Logger log = getLogger(NimbusHMACJwtProcessor.class);
    /**
     * jwt token ID 生成器,默认 UUID
     */
    private IdGenerator idGenerator;

    /**
     * Jwt token 服务
     * <p>
     * 采用默认 UUID 生成器、秘钥、生命时长进行初始化
     */
    public NimbusHMACJwtProcessor() {
        this.idGenerator = new UUIDIdGenerator();
    }

    /**
     * Jwt token 服务
     * <p>
     * 采用默认 秘钥、生命时长进行初始化
     */
    public NimbusHMACJwtProcessor(IdGenerator idGenerator) {
        this.idGenerator = idGenerator;
    }

    /**
     * 生成 jwt token
     *
     * @param userDetails the user details
     * @return the string
     */
    @Override
    public String generate(UserDetails userDetails) {
        HeimdallProperties config = ConfigManager.getConfig();
        final SimpleToken token = SimpleToken.build(idGenerator.generate(), config.getToken().getTimeout(), userDetails);
        log.debug("[generate]::userDetails = [{}],token = [{}]", userDetails, token);
        return generate(JacksonUtil.toJson(token));
    }

    /**
     * 校验 jwt token 合法性
     *
     * @param token 请求中获取到的 token
     * @return 通过则返回 SimpleToken 对象，失败，抛出异常
     * @see HeimdallInvalidJwtException
     */
    @Override
    public SimpleToken verify(String token) {

        if (StrUtils.isBlank(token)) {
            throw new HeimdallJwtException("token can not be null");
        }
        HeimdallProperties config = ConfigManager.getConfig();
        try {
            log.debug("[verify]::token = [{}]", token);
            //从token中解析JWS对象
            JWSObject jwsObject = JWSObject.parse(token);
            //创建HMAC验证器
            JWSVerifier jwsVerifier = new MACVerifier(Md5Util.encrypt(config.getJwt().getSecret()));
            if (!jwsObject.verify(jwsVerifier)) {
                throw new HeimdallInvalidJwtException();
            }
            String payload = jwsObject.getPayload().toString();
            SimpleToken payloadDto = JacksonUtil.toObject(payload, SimpleToken.class);
            if (payloadDto.hasExpired()) {
                throw new HeimdallExpiredJwtException();
            }
            log.debug("[verify]::token = [{}],payload = [{}]", token, payload);
            return payloadDto;
        } catch (ParseException |
                JOSEException e) {
            throw new HeimdallInvalidJwtException();
        }

    }

    /**
     * 生成 jwt token 串
     *
     * @param payloadStr the payload str
     * @return the string
     */
    @Override
    public String generate(String payloadStr) {
        try {
            log.debug("[generate]::payloadStr = [{}]", payloadStr);
            //创建JWS头，设置签名算法和类型
            JWSHeader jwsHeader = new JWSHeader.Builder(JWSAlgorithm.HS256).
                    type(JOSEObjectType.JWT)
                    .build();
            //将负载信息封装到Payload中
            Payload payload = new Payload(payloadStr);
            //创建JWS对象
            JWSObject jwsObject = new JWSObject(jwsHeader, payload);
            HeimdallProperties config = ConfigManager.getConfig();
            //创建HMAC签名器
            JWSSigner jwsSigner = new MACSigner(Md5Util.encrypt(config.getJwt().getSecret()));
            //签名
            jwsObject.sign(jwsSigner);
            log.debug("[generate]::payloadStr = [{}],jwtString = [{}]", payloadStr, jwsObject.serialize());
            return jwsObject.serialize();
        } catch (JOSEException e) {
            throw new HeimdallJwtException("Token generate failed", e);
        }
    }

    @Override
    public String generate(SimpleToken token) {
        return generate(JacksonUtil.toJson(token));
    }

    /**
     * 获取 id 生成器
     *
     * @return the token id generator
     */
    @Override
    public IdGenerator getIdGenerator() {
        return idGenerator;
    }

    /**
     * 设置 id 生成器
     *
     * @param idGenerator the token id generator
     * @return the token id generator
     */
    @Override
    public NimbusHMACJwtProcessor setIdGenerator(IdGenerator idGenerator) {
        this.idGenerator = idGenerator;
        return this;
    }

}
