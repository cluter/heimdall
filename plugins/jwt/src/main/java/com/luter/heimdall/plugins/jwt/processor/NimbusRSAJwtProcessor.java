/*
 *
 *  *    Copyright 2020-2021 luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.plugins.jwt.processor;

import com.luter.heimdall.core.config.ConfigManager;
import com.luter.heimdall.core.config.HeimdallProperties;
import com.luter.heimdall.core.details.UserDetails;
import com.luter.heimdall.core.jwt.JwtProcessor;
import com.luter.heimdall.core.token.SimpleToken;
import com.luter.heimdall.core.token.id.IdGenerator;
import com.luter.heimdall.core.token.id.UUIDIdGenerator;
import com.luter.heimdall.plugins.jwt.exception.HeimdallExpiredJwtException;
import com.luter.heimdall.plugins.jwt.exception.HeimdallInvalidJwtException;
import com.luter.heimdall.plugins.jwt.exception.HeimdallJwtException;
import com.luter.heimdall.plugins.jwt.util.JacksonUtil;
import com.luter.heimdall.plugins.jwt.util.RSAKeyUtil;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;

import java.text.ParseException;

/**
 * Nimbus FSA 算法生成 jwt token 实现
 * <p>
 * 这个实现生成的 jwt 字符串比较长
 *
 * @author luter
 */
public class NimbusRSAJwtProcessor implements JwtProcessor {
    /**
     * jwt token ID 生成器,默认 UUID
     */
    private IdGenerator idGenerator;

    /**
     * Instantiates a new Nimbus rsa jwt service.
     */
    public NimbusRSAJwtProcessor() {
        this.idGenerator = new UUIDIdGenerator();
    }

    /**
     * Instantiates a new Nimbus rsa jwt service.
     *
     * @param idGenerator the id generator
     */
    public NimbusRSAJwtProcessor(IdGenerator idGenerator) {
        this();
        this.idGenerator = idGenerator;
    }

    @Override
    public String generate(UserDetails userDetails) {
        HeimdallProperties config = ConfigManager.getConfig();
        final SimpleToken token = SimpleToken.build(idGenerator.generate(), config.getToken().getTimeout(), userDetails);
        return generate(JacksonUtil.toJson(token));
    }

    @Override
    public String generate(String payloadStr) {
        try {
            //创建JWS头，设置签名算法和类型
            JWSHeader jwsHeader = new JWSHeader.Builder(JWSAlgorithm.RS256)
                    .type(JOSEObjectType.JWT)
                    .build();
            //将负载信息封装到Payload中
            Payload payload = new Payload(payloadStr);
            //创建JWS对象
            JWSObject jwsObject = new JWSObject(jwsHeader, payload);
            //创建RSA签名器
            JWSSigner jwsSigner = new RSASSASigner(RSAKeyUtil.generateRSAKey());
            //签名
            jwsObject.sign(jwsSigner);
            return jwsObject.serialize();
        } catch (JOSEException e) {
            throw new HeimdallJwtException(e.getMessage(), e);
        }
    }

    @Override
    public String generate(SimpleToken token) {
        return generate(JacksonUtil.toJson(token));
    }

    @Override
    public SimpleToken verify(String tokenStr) {
        try {
            //从token中解析JWS对象
            JWSObject jwsObject = JWSObject.parse(tokenStr);
            //使用RSA公钥创建RSA验证器
            JWSVerifier jwsVerifier = new RSASSAVerifier(RSAKeyUtil.generateRSAKey().toPublicJWK());
            if (!jwsObject.verify(jwsVerifier)) {
                throw new HeimdallInvalidJwtException();
            }
            String payload = jwsObject.getPayload().toString();
            SimpleToken payloadDto = JacksonUtil.toObject(payload, SimpleToken.class);
            if (payloadDto.getExp() < System.currentTimeMillis()) {
                throw new HeimdallExpiredJwtException();
            }
            return payloadDto;
        } catch (JOSEException | ParseException e) {
            throw new HeimdallJwtException(e.getMessage(), e);
        }
    }

    @Override
    public Object generatePublicKey() {
        return RSAKeyUtil.generateRSAKey();
    }

    /**
     * Gets id generator.
     *
     * @return the id generator
     */
    @Override
    public IdGenerator getIdGenerator() {
        return idGenerator;
    }

    /**
     * Sets id generator.
     *
     * @param idGenerator the id generator
     * @return the id generator
     */
    @Override
    public NimbusRSAJwtProcessor setIdGenerator(IdGenerator idGenerator) {
        this.idGenerator = idGenerator;
        return this;
    }

}
