/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.plugins.jwt.util;/*
 *
 *  *    Copyright 2020-2021 luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

import com.luter.heimdall.plugins.jwt.exception.HeimdallJwtException;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator;

import java.util.UUID;

/**
 * RSA 秘钥工具类
 * <p>
 * 单例
 *
 * @author luter
 */
public final class RSAKeyUtil {
    /**
     * The constant rsaKey.
     */
    private static volatile RSAKey rsaKey;

    /**
     * Generate rsa key rsa key.
     *
     * @return the rsa key
     */
    public static RSAKey generateRSAKey() {
        if (null == rsaKey) {
            synchronized (RSAKeyUtil.class) {
                if (null == rsaKey) {
                    try {
                        rsaKey = new RSAKeyGenerator(2048)
                                .keyUse(KeyUse.SIGNATURE)
                                .keyID(UUID.randomUUID().toString())
                                .generate();
                    } catch (JOSEException e) {
                        throw new HeimdallJwtException(e.getMessage(), e);
                    }
                }
            }
        }
        return rsaKey;
    }

}
