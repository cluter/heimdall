/*
 *
 *  *    Copyright 2020-2021 luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.plugins.redis.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import com.luter.heimdall.core.authorization.authority.GrantedAuthority;
import com.luter.heimdall.core.token.SimpleToken;
import org.slf4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * 权限缓存 RedisTemplate  Bean 基础配置
 * <p>
 * Key 采用 String 序列化
 * <p>
 * value 采用Jackson2JsonRedisSerializer序列化
 * <p>
 * 继承此类进行配置
 *
 * @author luter
 */
public abstract class DefaultJacksonRedisTemplateConfiguration {
    /**
     * The constant log.
     */
    private static final transient Logger log = getLogger(DefaultJacksonRedisTemplateConfiguration.class);
    /**
     * The constant DEFAULT_DATE_TIME_FORMAT.
     */
    private static final String DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    /**
     * The constant DEFAULT_DATE_FORMAT.
     */
    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
    /**
     * The constant DEFAULT_TIME_FORMAT.
     */
    private static final String DEFAULT_TIME_FORMAT = "HH:mm:ss";

    /**
     * Token redis template redis template.
     *
     * @param factory the factory
     * @return the redis template
     */
    @Bean
    public RedisTemplate<String, SimpleToken> tokenRedisTemplate(RedisConnectionFactory factory) {
        RedisTemplate<String, SimpleToken> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(factory);
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        redisTemplate.setKeySerializer(stringRedisSerializer);
        redisTemplate.setHashKeySerializer(stringRedisSerializer);
        Jackson2JsonRedisSerializer<Object> jackson2JsonRedisSerializer = jackson2JsonRedisSerializer();
        redisTemplate.setValueSerializer(jackson2JsonRedisSerializer);
        redisTemplate.setHashValueSerializer(jackson2JsonRedisSerializer);
        redisTemplate.afterPropertiesSet();
        log.info("Default Token Redis Jackson Cache: RedisTemplate<String, SimpleToken> redisTemplate  Initialized.  ");
        return redisTemplate;
    }

    /**
     * 应用 权限 缓存 redis 配置
     *
     * @param factory the factory
     * @return the redis template
     */
    @Bean
    protected RedisTemplate<String, Map<String, List<String>>> appAuthoritiesRedisTemplate(RedisConnectionFactory factory) {
        RedisTemplate<String, Map<String, List<String>>> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(factory);
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        redisTemplate.setKeySerializer(stringRedisSerializer);
        redisTemplate.setHashKeySerializer(stringRedisSerializer);
        Jackson2JsonRedisSerializer<Object> jackson2JsonRedisSerializer = jackson2JsonRedisSerializer();
        redisTemplate.setValueSerializer(jackson2JsonRedisSerializer);
        redisTemplate.setHashValueSerializer(jackson2JsonRedisSerializer);
        redisTemplate.afterPropertiesSet();
        log.info("Default App Authorities Redis Jackson Cache: RedisTemplate<String, Collection<String>> redisTemplate Initialized.  ");
        return redisTemplate;
    }

    /**
     * 用户 权限缓存 redis 配置
     *
     * @param factory the factory
     * @return the redis template
     */
    @Bean
    protected RedisTemplate<String, List<? extends GrantedAuthority>> userAuthoritiesRedisTemplate(RedisConnectionFactory factory) {
        RedisTemplate<String, List<? extends GrantedAuthority>> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(factory);
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();

        redisTemplate.setKeySerializer(stringRedisSerializer);
        redisTemplate.setHashKeySerializer(stringRedisSerializer);
        Jackson2JsonRedisSerializer<Object> jackson2JsonRedisSerializer = jackson2JsonRedisSerializer();
        redisTemplate.setValueSerializer(jackson2JsonRedisSerializer);
        redisTemplate.setHashValueSerializer(jackson2JsonRedisSerializer);
        redisTemplate.afterPropertiesSet();
        log.info("Default User Authorities Redis Jackson Cache: RedisTemplate<String, List<? extends GrantedAuthority>>  redisTemplate Initialized. ");
        return redisTemplate;
    }

    /**
     * 采用 Jackson 值存储
     *
     * @return the jackson 2 json redis serializer
     */
    protected Jackson2JsonRedisSerializer<Object> jackson2JsonRedisSerializer() {
        Jackson2JsonRedisSerializer<Object> jackson2JsonRedisSerializer =
                new Jackson2JsonRedisSerializer<>(Object.class);
        ObjectMapper objectMapper = doInitObjectMapper(new ObjectMapper());
        objectMapper.configure(MapperFeature.USE_ANNOTATIONS, false);
        // 系列化到redis的数据包含类型，否则将会是纯的json，redis会以LinkHashMap返回，导致反序列化失败
        objectMapper.activateDefaultTyping(
                LaissezFaireSubTypeValidator.instance,
                ObjectMapper.DefaultTyping.NON_FINAL,
                JsonTypeInfo.As.PROPERTY);
        jackson2JsonRedisSerializer.setObjectMapper(objectMapper);
        return jackson2JsonRedisSerializer;
    }

    /**
     * 初始化 ObjectMapper 时间方法
     *
     * @param objectMapper the object mapper
     * @return the object mapper
     */
    protected static ObjectMapper doInitObjectMapper(ObjectMapper objectMapper) {
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        //不显示为null的字段
        objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        //枚举值处理
        objectMapper.enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);
        //不存在属性处理
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        // 过滤对象的null属性.始终包含null属性
        objectMapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);
        //忽略transient标记的字段
        objectMapper.enable(MapperFeature.PROPAGATE_TRANSIENT_MARKER);
        return registerModule(objectMapper);
    }

    /**
     * 为 ObjectMapper 注册模块，包括常见日期时间 参数名称，时区设置等
     *
     * @param objectMapper the object mapper
     * @return the object mapper
     */
    protected static ObjectMapper registerModule(ObjectMapper objectMapper) {
        // 指定时区
        objectMapper.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        // 日期类型字符串处理
        objectMapper.setDateFormat(new SimpleDateFormat(DEFAULT_DATE_TIME_FORMAT));
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer(DateTimeFormatter.ofPattern(DEFAULT_DATE_TIME_FORMAT)));
        simpleModule.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(DateTimeFormatter.ofPattern(DEFAULT_DATE_TIME_FORMAT)));
        simpleModule.addSerializer(LocalDate.class, new LocalDateSerializer(DateTimeFormatter.ofPattern(DEFAULT_DATE_FORMAT)));
        simpleModule.addDeserializer(LocalDate.class, new LocalDateDeserializer(DateTimeFormatter.ofPattern(DEFAULT_DATE_FORMAT)));
        simpleModule.addSerializer(LocalTime.class, new LocalTimeSerializer(DateTimeFormatter.ofPattern(DEFAULT_TIME_FORMAT)));
        simpleModule.addDeserializer(LocalTime.class, new LocalTimeDeserializer((DateTimeFormatter.ofPattern(DEFAULT_TIME_FORMAT))));
        simpleModule.addSerializer(BigInteger.class, ToStringSerializer.instance);
        simpleModule.addSerializer(Long.class, ToStringSerializer.instance);
        simpleModule.addSerializer(Long.TYPE, ToStringSerializer.instance);
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.registerModule(simpleModule);
        return objectMapper;
    }
}
