/*
 *
 *  *    Copyright 2020-2021 luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.plugins.redis.config;

import com.luter.heimdall.core.authorization.authority.GrantedAuthority;
import com.luter.heimdall.core.token.SimpleToken;
import org.slf4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.util.List;
import java.util.Map;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * 权限缓存 RedisTemplate  Bean 基础配置
 * <p>
 * Key 采用 stringRedisSerializer
 * <p>
 * value 默认 jdk 二进制序列化
 * <p>
 * 继承此类进行配置
 *
 * @author luter
 */
public abstract class DefaultStringKeyRedisTemplateConfiguration {
    /**
     * The constant log.
     */
    private static final transient Logger log = getLogger(DefaultStringKeyRedisTemplateConfiguration.class);

    /**
     * Token redis template redis template.
     *
     * @param factory the factory
     * @return the redis template
     */
    @Bean
    public RedisTemplate<String, SimpleToken> tokenRedisTemplate(RedisConnectionFactory factory) {
        RedisTemplate<String, SimpleToken> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(factory);
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        redisTemplate.setKeySerializer(stringRedisSerializer);
        redisTemplate.setHashKeySerializer(stringRedisSerializer);
        redisTemplate.afterPropertiesSet();
        log.info("Default Token Redis Jackson Cache: RedisTemplate<String, SimpleToken> redisTemplate  Initialized.  ");
        return redisTemplate;
    }

    /**
     * 应用 权限 缓存 redis 配置
     *
     * @param factory the factory
     * @return the redis template
     */
    @Bean
    protected RedisTemplate<String, Map<String, List<String>>> appAuthoritiesRedisTemplate(RedisConnectionFactory factory) {
        RedisTemplate<String, Map<String, List<String>>> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(factory);
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        redisTemplate.setKeySerializer(stringRedisSerializer);
        redisTemplate.setHashKeySerializer(stringRedisSerializer);
        redisTemplate.afterPropertiesSet();
        log.info("Default App Authorities Redis Jackson Cache: RedisTemplate<String, Collection<String>> redisTemplate Initialized.  ");
        return redisTemplate;
    }

    /**
     * 用户 权限缓存 redis 配置
     *
     * @param factory the factory
     * @return the redis template
     */
    @Bean
    protected RedisTemplate<String, List<? extends GrantedAuthority>> userAuthoritiesRedisTemplate(RedisConnectionFactory factory) {
        RedisTemplate<String, List<? extends GrantedAuthority>> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(factory);
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        redisTemplate.setKeySerializer(stringRedisSerializer);
        redisTemplate.setHashKeySerializer(stringRedisSerializer);
        redisTemplate.afterPropertiesSet();
        log.info("Default User Authorities Redis Jackson Cache: RedisTemplate<String, List<? extends GrantedAuthority>>  redisTemplate Initialized. ");
        return redisTemplate;
    }

}
