/*
 *
 *  *    Copyright 2020-2021 luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.plugins.thymeleaf.dialect;


import com.luter.heimdall.core.authorization.handler.HeimdallMethodAuthorizationHandler;
import com.luter.heimdall.core.manager.AuthorizationManager;
import com.luter.heimdall.plugins.thymeleaf.processor.attribute.*;
import com.luter.heimdall.plugins.thymeleaf.processor.element.*;
import org.slf4j.Logger;
import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.processor.IProcessor;
import org.thymeleaf.standard.StandardDialect;

import java.util.LinkedHashSet;
import java.util.Set;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * thymeleaf 认证授权标签方言
 *
 * @author luter
 */

public class ThymeleafDialect extends AbstractProcessorDialect {
    /**
     * The constant log.
     */
    private static final transient Logger log = getLogger(ThymeleafDialect.class);
    /**
     * The constant NAME.
     */
    private static final String NAME = "Auth";
    /**
     * The constant PREFIX.
     */
    private static final String PREFIX = "auth";

    /**
     * The Authorization manager.
     */
    private AuthorizationManager authorizationManager;

    /**
     * thymeleaf 认证授权标签方言
     *
     * @param authorizationManager the authorization manager
     */
    public ThymeleafDialect(AuthorizationManager authorizationManager) {
        super(NAME, PREFIX, StandardDialect.PROCESSOR_PRECEDENCE);
        this.authorizationManager = authorizationManager;
    }

    @Override
    public Set<IProcessor> getProcessors(String dialectPrefix) {
        if (null == authorizationManager) {
            log.warn("[ThymeleafDialect->getProcessors]::" +
                    "The AuthorizationManager is not proper set ,any of Thymeleaf Heimdall tag will be invalid. " +
                    "dialectPrefix = [{}]", dialectPrefix);
            return new LinkedHashSet<>();
        } else {
            return createStandardProcessorsSet(dialectPrefix);
        }

    }

    /**
     * Create standard processors set.
     *
     * @param dialectPrefix the dialect prefix
     * @return the set
     */
    @SuppressWarnings("MapOrSetKeyShouldOverrideHashCodeEquals")
    private Set<IProcessor> createStandardProcessorsSet(String dialectPrefix) {
        LinkedHashSet<IProcessor> processors = new LinkedHashSet<>();
        //已认证
        processors.add(new AuthenticatedAttrProcessor(dialectPrefix)
                .condition(() -> authorizationManager.getAuthenticationManager()
                        .isAuthenticated(false)));
        processors.add(new AuthenticatedElementProcessor(dialectPrefix)
                .condition(() -> authorizationManager.getAuthenticationManager()
                        .isAuthenticated(false)));

        //未认证
        processors.add(new NotAuthenticatedAttrProcessor(dialectPrefix)
                .condition(() -> authorizationManager
                        .getAuthenticationManager().isAuthenticated(false)));
        processors.add(new NotAuthenticatedElementProcessor(dialectPrefix).condition(
                () -> authorizationManager.getAuthenticationManager().isAuthenticated(false)
        ));

        ///当前登录用户的 token 信息
        processors.add(new TokenAttrProcessor(dialectPrefix).supply(()
                -> authorizationManager.getAuthenticationManager().getCurrentToken(false)));
        processors.add(new TokenElementProcessor(dialectPrefix).supply(()
                -> authorizationManager.getAuthenticationManager().getCurrentToken(false)));

        ///当前登录用户的 userDetails 信息
        processors.add(new UserDetailAttrProcessor(dialectPrefix).supply(()
                -> authorizationManager.getAuthenticationManager().getCurrentUser(false)));
        processors.add(new UserDetailElementProcessor(dialectPrefix).supply(()
                -> authorizationManager.getAuthenticationManager().getCurrentUser(false)));

        //不具备多个权限中的任何一个
        processors.add(new LacksPermsAttrProcessor(dialectPrefix).condition(
                perms -> HeimdallMethodAuthorizationHandler.hasAnyPermissions(authorizationManager, perms)));
        processors.add(new LacksPermsElementProcessor(dialectPrefix).condition(
                perms -> HeimdallMethodAuthorizationHandler.hasAnyPermissions(authorizationManager, perms)));

        //同时具备一个或者多个权限
        processors.add(new HasPermsAttrProcessor(dialectPrefix).condition(strings
                -> HeimdallMethodAuthorizationHandler.hasAllPermissions(authorizationManager, strings)));
        processors.add(new HasPermsElementProcessor(dialectPrefix).condition(strings
                -> HeimdallMethodAuthorizationHandler.hasAllPermissions(authorizationManager, strings)));

        //具备多个权限其中之一
        processors.add(new HasAnyPermsAttrProcessor(dialectPrefix).condition(strings
                -> HeimdallMethodAuthorizationHandler.hasAnyPermissions(authorizationManager, strings)));
        processors.add(new HasAnyPermsElementProcessor(dialectPrefix).condition(strings
                -> HeimdallMethodAuthorizationHandler.hasAnyPermissions(authorizationManager, strings)));

        //不具备多个角色中的任何一个角色
        processors.add(new LacksRolesAttrProcessor(dialectPrefix).condition(strings
                -> HeimdallMethodAuthorizationHandler.hasAnyPermissions(authorizationManager, strings)));
        processors.add(new LacksRolesElementProcessor(dialectPrefix).condition(strings
                -> HeimdallMethodAuthorizationHandler.hasAnyPermissions(authorizationManager, strings)));

        //同时具备一个或者多个角色
        processors.add(new HasRolesAttrProcessor(dialectPrefix).condition(strings
                -> HeimdallMethodAuthorizationHandler.hasAllPermissions(authorizationManager, strings)));
        processors.add(new HasRolesElementProcessor(dialectPrefix).condition(strings
                -> HeimdallMethodAuthorizationHandler.hasAllPermissions(authorizationManager, strings)));

        //具备多个角色其中之一
        processors.add(new HasAnyRolesAttrProcessor(dialectPrefix).condition(strings
                -> HeimdallMethodAuthorizationHandler.hasAnyPermissions(authorizationManager, strings)));
        processors.add(new HasAnyRolesElementProcessor(dialectPrefix).condition(strings
                -> HeimdallMethodAuthorizationHandler.hasAnyPermissions(authorizationManager, strings)));

        return processors;
    }

    public AuthorizationManager getAuthorizationManager() {
        return authorizationManager;
    }

    public ThymeleafDialect setAuthorizationManager(AuthorizationManager authorizationManager) {
        this.authorizationManager = authorizationManager;
        return this;
    }
}
