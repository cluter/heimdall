/*
 *
 *  *    Copyright 2020-2021 luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.plugins.thymeleaf.processor;

/**
 * The interface Attr name constants.
 *
 * @author luter
 */
public interface AttrNameConstants {
    /**
     * 当前登录用户的 userDetails 对象
     *
     * @author luter
     */
    interface UserDetailsAttr {
        /**
         * The constant ATTRIBUTE.
         */
        String ATTRIBUTE = "user";
        /**
         * The constant ELEMENT.
         */
        String ELEMENT = "user";
    }

    /**
     * 当前登录用户的 token对象
     *
     * @author luter
     */
    interface TokenAttr {
        /**
         * The constant ATTRIBUTE.
         */
        String ATTRIBUTE = "token";
        /**
         * The constant ELEMENT.
         */
        String ELEMENT = "token";
    }

    /**
     * 已登录
     *
     * @author luter
     */
    interface Authenticated {
        /**
         * The constant ATTRIBUTE.
         */
        String ATTRIBUTE = "authenticated";
        /**
         * The constant ELEMENT.
         */
        String ELEMENT = "authenticated";
    }

    /**
     * 未登录
     *
     * @author luter
     */
    interface NotAuthenticated {
        /**
         * The constant ATTRIBUTE.
         */
        String ATTRIBUTE = "notAuthenticated";
        /**
         * The constant ELEMENT.
         */
        String ELEMENT = "notauthenticated";
    }

    /**
     * 具有多个权限中的一个
     *
     * @author luter
     */
    interface HasAnyPerms {
        /**
         * The constant ATTRIBUTE.
         */
        String ATTRIBUTE = "hasAnyPerms";
        /**
         * The constant ELEMENT.
         */
        String ELEMENT = "hasanyperms";
    }

    /**
     * 具有多个角色中的一个
     *
     * @author luter
     */
    interface HasAnyRoles {
        /**
         * The constant ATTRIBUTE.
         */
        String ATTRIBUTE = "hasAnyRoles";
        /**
         * The constant ELEMENT.
         */
        String ELEMENT = "hasanyroles";
    }

    /**
     * 同时具有多个一个或者多个权限
     *
     * @author luter
     */
    interface HasPerms {
        /**
         * The constant ATTRIBUTE.
         */
        String ATTRIBUTE = "hasPerms";
        /**
         * The constant ELEMENT.
         */
        String ELEMENT = "hasperms";
    }

    /**
     * 同时具备一个或者多个角色
     *
     * @author luter
     */
    interface HasRoles {
        /**
         * The constant ATTRIBUTE.
         */
        String ATTRIBUTE = "hasRoles";
        /**
         * The constant ELEMENT.
         */
        String ELEMENT = "hasroles";
    }

    /**
     * 缺少多个权限中的某一个
     *
     * @author luter
     */
    interface LacksPerms {
        /**
         * The constant ATTRIBUTE.
         */
        String ATTRIBUTE = "lacksPerms";
        /**
         * The constant ELEMENT.
         */
        String ELEMENT = "lacksperms";
    }

    /**
     * 缺少多个角色中的某一个
     *
     * @author luter
     */
    interface LacksRoles {
        /**
         * The constant ATTRIBUTE.
         */
        String ATTRIBUTE = "lacksRoles";
        /**
         * The constant ELEMENT.
         */
        String ELEMENT = "lacksroles";
    }
}
