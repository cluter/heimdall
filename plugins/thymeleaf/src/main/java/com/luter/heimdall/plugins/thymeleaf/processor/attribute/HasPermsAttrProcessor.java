/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.plugins.thymeleaf.processor.attribute;

import com.luter.heimdall.plugins.thymeleaf.processor.AttrNameConstants;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractAttributeTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.templatemode.TemplateMode;

import java.util.function.Predicate;

import static com.luter.heimdall.plugins.thymeleaf.util.ThymeleafFacade.getRawValue;

/**
 * 同时具备多个权限
 *
 * @author luter
 */
public class HasPermsAttrProcessor extends AbstractAttributeTagProcessor {
    /**
     * The constant DELIMITER.
     */
    private static final String DELIMITER = ",";
    /**
     * The constant ATTRIBUTE_NAME.
     */
    private static final String ATTRIBUTE_NAME = AttrNameConstants.HasPerms.ATTRIBUTE;
    /**
     * The constant PRECEDENCE.
     */
    private static final int PRECEDENCE = 300;
    private Predicate<String[]> condition;

    public HasPermsAttrProcessor condition(Predicate<String[]> condition) {
        this.condition = condition;
        return this;
    }
    /**
     * 同时具备多个权限
     *
     * @param dialectPrefix 前缀
     */
    public HasPermsAttrProcessor(String dialectPrefix) {
        super(
                TemplateMode.HTML,
                dialectPrefix,
                null,
                false,
                ATTRIBUTE_NAME,
                true,
                PRECEDENCE,
                true);
    }

    @Override
    protected void doProcess(ITemplateContext iTemplateContext,
                             IProcessableElementTag iProcessableElementTag,
                             AttributeName attributeName,
                             String attributeValue,
                             IElementTagStructureHandler iElementTagStructureHandler) {
        String rawValue = getRawValue(iProcessableElementTag, attributeName);
        String[] values = rawValue.split(DELIMITER);
        if (condition.test(values)) {
            iElementTagStructureHandler.removeAttribute(attributeName);
        } else {
            iElementTagStructureHandler.removeElement();
        }
    }
}