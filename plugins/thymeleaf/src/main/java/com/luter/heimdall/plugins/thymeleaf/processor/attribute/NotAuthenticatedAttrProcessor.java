/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.plugins.thymeleaf.processor.attribute;

import com.luter.heimdall.plugins.thymeleaf.processor.AttrNameConstants;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractAttributeTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.templatemode.TemplateMode;

import java.util.function.BooleanSupplier;

/**
 * The type Not authenticated attr processor.
 *
 * @author luter
 */
public class NotAuthenticatedAttrProcessor extends AbstractAttributeTagProcessor {
    /**
     * The constant ATTRIBUTE_NAME.
     */
    private static final String ATTRIBUTE_NAME = AttrNameConstants.NotAuthenticated.ATTRIBUTE;
    /**
     * The constant PRECEDENCE.
     */
    private static final int PRECEDENCE = 300;
    private BooleanSupplier booleanSupplier;

    public NotAuthenticatedAttrProcessor condition(BooleanSupplier booleanSupplier) {
        this.booleanSupplier = booleanSupplier;
        return this;
    }

    /**
     * Instantiates a new Not authenticated attr processor.
     *
     * @param dialectPrefix the dialect prefix
     */
    public NotAuthenticatedAttrProcessor(String dialectPrefix) {
        super(
                TemplateMode.HTML,
                dialectPrefix,
                null,
                false,
                ATTRIBUTE_NAME,
                true,
                PRECEDENCE,
                true);
    }

    @Override
    protected void doProcess(ITemplateContext iTemplateContext,
                             IProcessableElementTag iProcessableElementTag,
                             AttributeName attributeName,
                             String attributeValue,
                             IElementTagStructureHandler iElementTagStructureHandler) {
        if (!booleanSupplier.getAsBoolean()) {
            iElementTagStructureHandler.removeAttribute(attributeName);
        } else {
            iElementTagStructureHandler.removeElement();
        }
    }
}