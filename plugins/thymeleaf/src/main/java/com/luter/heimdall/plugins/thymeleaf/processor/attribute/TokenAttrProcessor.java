/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.plugins.thymeleaf.processor.attribute;

import com.luter.heimdall.core.token.SimpleToken;
import com.luter.heimdall.plugins.thymeleaf.processor.AttrNameConstants;
import com.luter.heimdall.plugins.thymeleaf.util.ThymeleafFacade;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.model.IModel;
import org.thymeleaf.model.IModelFactory;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractAttributeTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.templatemode.TemplateMode;
import org.unbescape.html.HtmlEscape;

import java.util.function.Supplier;

/**
 * 获取 token 信息
 * <p>
 *
 * @author luter
 */
public final class TokenAttrProcessor extends AbstractAttributeTagProcessor {
    /**
     * The constant ATTRIBUTE_NAME.
     */
    private static final String ATTRIBUTE_NAME = AttrNameConstants.TokenAttr.ATTRIBUTE;
    /**
     * The constant PRECEDENCE.
     */
    private static final int PRECEDENCE = 300;

    private Supplier<SimpleToken> supplier;

    public TokenAttrProcessor supply(Supplier<SimpleToken> function) {
        this.supplier = function;
        return this;
    }

    /**
     * 获取 token 信息
     * <p>
     * eg:
     * "<p auth:token property="appId"></p>"
     *
     * @param dialectPrefix the dialect prefix
     * @author luter
     */
    public TokenAttrProcessor(String dialectPrefix) {
        super(
                TemplateMode.HTML,
                dialectPrefix,
                null,
                false,
                ATTRIBUTE_NAME,
                true,
                PRECEDENCE,
                true);
    }

    @Override
    protected void doProcess(ITemplateContext iTemplateContext,
                             IProcessableElementTag iProcessableElementTag,
                             AttributeName attributeName,
                             String attributeValue,
                             IElementTagStructureHandler iElementTagStructureHandler) {
        final String property = iProcessableElementTag.getAttributeValue("property");
        final SimpleToken token = supplier.get();
        String text;
        if (property != null && null != token) {
            text = ThymeleafFacade.getPlainObjectProperty(token, property);
        } else {
            text = (null == token) ? "" : token.getId();
        }
        String elementCompleteName = iProcessableElementTag.getElementCompleteName();
        IModelFactory modelFactory = iTemplateContext.getModelFactory();
        IModel model = modelFactory.createModel();
        model.add(modelFactory.createOpenElementTag(elementCompleteName));
        model.add(modelFactory.createText(HtmlEscape.escapeHtml5(text)));
        model.add(modelFactory.createCloseElementTag(elementCompleteName));
        iElementTagStructureHandler.replaceWith(model, false);
    }

}