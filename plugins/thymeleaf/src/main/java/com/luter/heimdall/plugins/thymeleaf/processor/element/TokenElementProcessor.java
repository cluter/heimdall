/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.plugins.thymeleaf.processor.element;

import com.luter.heimdall.core.token.SimpleToken;
import com.luter.heimdall.plugins.thymeleaf.processor.AttrNameConstants;
import com.luter.heimdall.plugins.thymeleaf.util.ThymeleafFacade;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractElementTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.templatemode.TemplateMode;

import java.util.function.Supplier;

/**
 * 获取 token 信息
 * <p>
 * eg:
 * <p>
 * &lt;auth:token property="attributes" &gt; &lt;/auth:token &gt;
 * </p>
 *
 * @author luter
 */
public class TokenElementProcessor extends AbstractElementTagProcessor {
    /**
     * The constant ELEMENT_NAME.
     */
    private static final String ELEMENT_NAME = AttrNameConstants.TokenAttr.ELEMENT;
    /**
     * The constant PRECEDENCE.
     */
    private static final int PRECEDENCE = 300;

    private Supplier<SimpleToken> supplier;

    public TokenElementProcessor supply(Supplier<SimpleToken> function) {
        this.supplier = function;
        return this;
    }

    /**
     * 获取 principal 信息
     * <p>
     * eg:
     * <p>
     * &lt;auth:token property="attributes" &gt; &lt;/auth:token &gt;
     * </p>
     *
     * @param dialectPrefix the dialect prefix
     * @author luter
     */
    public TokenElementProcessor(String dialectPrefix) {
        super(
                TemplateMode.HTML,
                dialectPrefix,
                ELEMENT_NAME,
                true,
                null,
                false,
                PRECEDENCE);
    }

    @Override
    protected void doProcess(ITemplateContext iTemplateContext,
                             IProcessableElementTag iProcessableElementTag,
                             IElementTagStructureHandler iElementTagStructureHandler) {
        final String property = iProcessableElementTag.getAttributeValue("property");
        final SimpleToken token = supplier.get();
        String text;
        if (property != null && null != token) {
            text = ThymeleafFacade.getPlainObjectProperty(token, property);
        } else {
            text = (null == token) ? "" : token.getId();
        }
        iElementTagStructureHandler.replaceWith(text, false);
    }
}