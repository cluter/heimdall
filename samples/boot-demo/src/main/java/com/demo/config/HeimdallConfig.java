/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.demo.config;

import com.luter.heimdall.core.authorization.provider.DocumentAuthorityDataProvider;
import com.luter.heimdall.core.authorization.store.AuthorizationStore;
import com.luter.heimdall.core.authorization.store.MemoryCachedAuthorizationStore;
import com.luter.heimdall.core.context.WebContextHolder;
import com.luter.heimdall.core.manager.AuthenticationManager;
import com.luter.heimdall.core.manager.AuthorizationManager;
import com.luter.heimdall.core.manager.HeimdallAuthenticationManager;
import com.luter.heimdall.core.manager.HeimdallAuthorizationManager;
import com.luter.heimdall.core.token.store.MemoryCachedTokenStore;
import com.luter.heimdall.core.token.store.TokenStore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
public class HeimdallConfig {


    @Bean
    public AuthorizationManager authorizationManager(WebContextHolder webContextHolder) {
        //配置权限Store，基于内存缓存
        AuthorizationStore authorizationStore = new MemoryCachedAuthorizationStore()
                //权限数据提供者，采用配置文件提供者，正式环境一般是来自数据库
                .dataProvider(new DocumentAuthorityDataProvider());
        //配置 TokenStore
        TokenStore tokenStore = new MemoryCachedTokenStore().authorizationStore(authorizationStore);
        //配置认证管理器
        AuthenticationManager authenticationManager =
                //web 上下文
                new HeimdallAuthenticationManager(webContextHolder)
                        //token Store
                        .tokenStore(tokenStore);
        //配置授权管理器
        return new HeimdallAuthorizationManager(authorizationStore, authenticationManager);
    }
}
