/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.samples.jfinal;

import com.jfinal.aop.Aop;
import com.jfinal.aop.AopManager;
import com.jfinal.config.*;
import com.jfinal.server.undertow.UndertowServer;
import com.jfinal.template.Engine;
import com.luter.heimdall.samples.jfinal.config.HeimdallManagerCreator;
import com.luter.heimdall.samples.jfinal.service.CatService;
import com.luter.heimdall.core.limiter.MemoryCachedPasswordRetryLimiter;
import com.luter.heimdall.core.manager.HeimdallAuthorizationManager;
import com.luter.heimdall.starter.jfinal.config.AbstractHeimdallJFinalConfig;
import com.luter.heimdall.starter.jfinal.interceptor.HeimdallJFinalAnnotationInterceptor;
import com.luter.heimdall.starter.jfinal.interceptor.HeimdallJFinalAuthorizeInterceptor;

/**
 * The type Jfinal app.
 *
 * @author luter
 */
public class JFinalDemoApp extends AbstractHeimdallJFinalConfig {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        UndertowServer.start(JFinalDemoApp.class, 8080, true);
    }

    @Override
    public void configConstant(Constants me) {
        me.setDevMode(false);
        me.setInjectDependency(true);
        me.setInjectSuperClass(true);
        //全局注册个服务做inject 测试的
        AopManager.me().addSingletonObject(new CatService());
        //密码重试限制
        AopManager.me().addSingletonObject(new MemoryCachedPasswordRetryLimiter());
        //认证授权管理器注册
        AopManager.me().addSingletonObject(HeimdallManagerCreator.initManager());
    }

    @Override
    public void configRoute(Routes routes) {
        //往这里扫：）
        routes.scan("com.luter.heimdall.samples.jfinal.controller");
    }

    @Override
    public void configEngine(Engine engine) {

    }

    @Override
    public void configPlugin(Plugins plugins) {

    }

    @Override
    public void configInterceptor(Interceptors interceptors) {
        //web context 上下文通过父类注入
        super.configInterceptor(interceptors);
        //认证授权管理器
        final HeimdallAuthorizationManager authManager = Aop.get(HeimdallAuthorizationManager.class);
        //权限拦截器
        interceptors.add(new HeimdallJFinalAuthorizeInterceptor(authManager));
        //注解拦截器
        interceptors.add(new HeimdallJFinalAnnotationInterceptor(authManager));
    }

    @Override
    public void configHandler(Handlers handlers) {

    }
}
