/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.samples.jfinal.controller;

import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.core.Path;
import com.luter.heimdall.samples.jfinal.service.CatService;

/**
 * The type Cat controller.
 *
 * @author luter
 */
@Path("/cat")
public class CatController extends Controller {

    /**
     * The Cat service.
     */
    @Inject
    private CatService catService;

    /**
     * List.
     */
    public void list() {
        final Integer c = getParaToInt("c", 10);
        renderJson(catService.getCatList(c));
    }

    /**
     * Detail.
     */
    public void detail() {
        renderText("详情");
    }

    /**
     * Save.
     */
    public void save() {
        renderText("新增");
    }

    /**
     * Update.
     */
    public void update() {
        renderText("修改");
    }

    /**
     * Delete.
     */
    public void delete() {
        renderText("删除");
    }
}
