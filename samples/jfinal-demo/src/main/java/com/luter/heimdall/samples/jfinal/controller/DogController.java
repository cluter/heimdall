/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.samples.jfinal.controller;

import com.jfinal.core.Controller;
import com.jfinal.core.Path;
import com.luter.heimdall.core.annotation.RequiresRole;

/**
 * The type Dog controller.
 *
 * @author luter
 */
@Path("/dog")
public class DogController extends Controller {
    /**
     * List.
     */
    @RequiresRole("admin")
    public void list() {
        final Integer c = getParaToInt("c", 10);
        renderJson(c + "只狗");
    }

    /**
     * Detail.
     */
    @RequiresRole("user")
    public void detail() {
        final Integer c = getParaToInt("c", 10);
        renderJson(c + "只狗");
    }
}
