/*
 *
 *  *    Copyright 2020-2021 luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.example.servletdemo.config;

import com.luter.heimdall.core.exception.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(value = {
            HeimdallUnauthenticatedException.class,
            HeimdallExpiredTokenException.class,
            HeimdallInvalidTokenException.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public void unAuthentication(HttpServletResponse response, HeimdallException e) throws IOException {
        response.sendError(HttpStatus.UNAUTHORIZED.value(), e.getMessage());
    }

    @ExceptionHandler(value = {HeimdallUnauthorizedException.class})
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public void unAuthorizedException(HttpServletResponse response, HeimdallUnauthorizedException e) throws IOException {
        response.sendError(HttpStatus.FORBIDDEN.value(), e.getMessage());
    }

    @ExceptionHandler(value = {HeimdallException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public void HeimdallException(HttpServletResponse response, HeimdallException e) throws IOException {
        response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
    }
}
