/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.example.servletdemo.config;

import com.luter.heimdall.boot.web.configurer.AbstractAuthWebMvcConfigurer;
import com.luter.heimdall.boot.web.interceptor.HeimdallAuthorizeInterceptor;
import com.luter.heimdall.core.manager.AuthorizationManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;

@Configuration
public class MvcConfig extends AbstractAuthWebMvcConfigurer {
    @Autowired
    private AuthorizationManager authorizationManager;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        super.addInterceptors(registry);
        final HeimdallAuthorizeInterceptor authInterceptor = new HeimdallAuthorizeInterceptor(authorizationManager);
        //路由权限拦截器
        registry.addInterceptor(authInterceptor).addPathPatterns("/**").order(-1);
    }
}
