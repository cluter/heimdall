/*
 *
 *  *    Copyright 2020-2021 luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.example.servletdemo.controller;

import com.luter.heimdall.core.annotation.*;
import com.luter.heimdall.core.token.SimpleToken;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/anno")
public class AnnotationController {

    @GetMapping("/list")
    @RequiresPermission("catList")
    public Object list(@CurrentToken SimpleToken token) {
        return token;
    }

    @GetMapping("/detail")
    @RequiresPermissions(value = {"catList", "catDetail"}, logical = Logical.ANY)
    public Object detail(@RequestParam(name = "id", defaultValue = "默认的") String id) {
        return "获取数据详情成功: ID 参数:" + id;
    }

    @PostMapping("/save")
    @RequiresPermission("catSave")
    public Object save() {
        return "保存数据成功";
    }

    @PostMapping("/update")
    @RequiresPermission("catUpdate")
    public Object update() {
        return "修改数据成功";
    }

    @PostMapping("/delete")
    @RequiresRole("catDelete")
    public Object delete(@RequestParam(name = "id", defaultValue = "默认的") String id) {
        return "删除数据成功: ID 参数:" + id;
    }

}
