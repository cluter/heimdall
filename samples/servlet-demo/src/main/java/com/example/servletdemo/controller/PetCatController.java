/*
 *
 *  *    Copyright 2020-2021 luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.example.servletdemo.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/pet/cat")
public class PetCatController {

    @GetMapping({"", "/"})
    public String toIndex() {
        return "pages/cat";
    }

    @GetMapping("/list")
    @ResponseBody
    public Object list() {
        return "获取数据列表成功";
    }

    @GetMapping("/detail")
    @ResponseBody
    public Object detail(@RequestParam(name = "id", defaultValue = "默认的") String id) {
        return "获取数据详情成功: ID 参数:" + id;
    }

    @PostMapping("/save")
    @ResponseBody
    public Object save() {
        return "保存数据成功";
    }

    @PostMapping("/update")
    @ResponseBody
    public Object update() {
        return "修改数据成功";
    }

    @PostMapping("/delete")
    @ResponseBody
    public Object delete(@RequestParam(name = "id", defaultValue = "默认的") String id) {
        return "删除数据成功: ID 参数:" + id;
    }

}
