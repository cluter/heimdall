/*
 *
 *  *    Copyright 2020-2021 luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.example.servletdemo.service;

import com.luter.heimdall.core.authorization.provider.DocumentUsersDataProvider;
import com.luter.heimdall.core.authorization.provider.model.UserDO;
import com.luter.heimdall.core.exception.HeimdallException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 用户信息获取
 *
 * @author luter
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class SysUserService {

    /**
     * 用户信息获取
     *
     * @param username the username
     * @return the user by name
     */
    public UserDO getUserByName(String username) {
        //从配置文件加载用户信息
        final UserDO userDO = DocumentUsersDataProvider.loadUserByUserName(username);
        if (null == userDO) {
            throw new HeimdallException("Wrong user name or password:nonExisted");
        }
        return userDO;
    }

}
