///*
// *
// *  *    Copyright 2020-2021 luter.me
// *  *
// *  *    Licensed under the Apache License, Version 2.0 (the "License");
// *  *    you may not use this file except in compliance with the License.
// *  *    You may obtain a copy of the License at
// *  *
// *  *      http://www.apache.org/licenses/LICENSE-2.0
// *  *
// *  *    Unless required by applicable law or agreed to in writing, software
// *  *    distributed under the License is distributed on an "AS IS" BASIS,
// *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// *  *    See the License for the specific language governing permissions and
// *  *    limitations under the License.
// *
// */
//
//package com.example.servletdemo.util;
//
//import com.example.servletdemo.dto.SysResourceDTO;
//import com.example.servletdemo.dto.SysUserDTO;
//import com.luter.heimdall.core.authorization.authority.GrantedAuthority;
//import com.luter.heimdall.core.authorization.authority.SimpleGrantedAuthority;
//import com.luter.heimdall.core.exception.HeimdallException;
//import com.luter.heimdall.core.utils.StrUtils;
//import com.luter.heimdall.core.utils.crypto.Md5Util;
//
//import java.util.*;
//import java.util.stream.Collectors;
//
///**
// * 模拟数据工具类
// * <p>
// * 系统权限以Map<String,String>形式存储在缓存里。
// * Map.key= url
// * Map.value = perm
// * <p>
// * 对于精确url授权，要求Url唯一，也就是Key不可以重复
// * <p>
// * 对于restful形式授权，key+method不可以重复。
// * <p>
// * 由于是以Map形式保存在缓存里，所以重复key只会保留一个
// * <p>
// * 这种方式下，key就相当于一个拦截规则，也就是说
// * 只要看到有这个url，就进入授权逻辑
// * <p>
// * 对于角色授权
// * <p>
// * 系统权限数据通过角色-资源关系表关联获取，这种情况下就会出现一个 url 属于多个角色的情况。
// * 所以权限拦截规则需要对关系数据按照 url 分组处理
// * <p>
// * 权限匹配的时候，通过 contains 进行判断，只要具备多个角色中的一个，就具有权限
// *
// * @author Luter
// */
//public final class DataUtil {
//
//    public static final String CIPHER_PASSWORD = encode("aaaaaa");
//
//    /**
//     * 模拟 用户表 数据
//     *
//     * @return the simple user list
//     */
//    public static List<SysUserDTO> getUserList() {
//        List<SysUserDTO> userList = new ArrayList<>();
//        //系统用户
//        userList.add(new SysUserDTO(1L, "admin", "17777777777", CIPHER_PASSWORD, true));
//        userList.add(new SysUserDTO(2L, "luter", "18888888888", CIPHER_PASSWORD, true));
//        return userList;
//    }
//
//    /**
//     * 模拟系统 资源表 数据
//     * <p>
//     * 一个唯一资源 对应一个唯一 perm
//     *
//     * @return the simple resource list
//     */
//    public static List<SysResourceDTO> getExactUrlResourceList() {
//        List<SysResourceDTO> resourceList = new ArrayList<>();
//        //路由
//        resourceList.add(new SysResourceDTO(1L, "新增数据", null, "/pet/cat/save", "catSave"));
//        resourceList.add(new SysResourceDTO(2L, "查看列表", null, "/pet/cat/list", "catList"));
//        resourceList.add(new SysResourceDTO(3L, "删除数据", null, "/pet/cat/delete", "catDelete"));
//        resourceList.add(new SysResourceDTO(4L, "修改数据", null, "/pet/cat/update", "catUpdate"));
//        resourceList.add(new SysResourceDTO(5L, "查看详情", null, "/pet/cat/detail", "catDetail"));
//        //页面
//        resourceList.add(new SysResourceDTO(6L, "在线用户", null, "/online", "online"));
//
//        resourceList.add(new SysResourceDTO(7L, "路由授权", null, "/pet/cat", "cat"));
//        resourceList.add(new SysResourceDTO(8L, "注解授权", null, "/anno", "anno"));
//        resourceList.add(new SysResourceDTO(9L, "权限 tag", null, "/tag", "tag"));
//        resourceList.add(new SysResourceDTO(10L, "首页", null, "/", "index"));
//        resourceList.add(new SysResourceDTO(11L, "首页", null, "/index", "index"));
//
//        resourceList.add(new SysResourceDTO(12L, "在线用户", null, "/kick/*", "kickUser"));
//        return resourceList;
//    }
//
//    /**
//     * 精确匹配路由 Url 的系统权限
//     *
//     * @return the exact url perms
//     */
//    public static Map<String, List<String>> getExactUrlPerms() {
//
//        final List<SysResourceDTO> resources = DataUtil.getExactUrlResourceList();
//        Map<String, List<String>> perms = new LinkedHashMap<>(resources.size());
//        //实际使用的时候，需要对 url 和 perm 进行校验
//        for (SysResourceDTO resource : resources) {
//            List<String> p = new ArrayList<>(1);
//            p.add(resource.getPerm());
//            perms.put(resource.getUrl(), p);
//        }
//        return perms;
//    }
//
//    /**
//     * 获取精确路由 url 的 用户权限
//     *
//     * @param principal the principal
//     * @return the exact user perms
//     */
//    public static List<? extends GrantedAuthority> getExactUserPerms(String principal) {
//        final List<SysResourceDTO> resources = DataUtil.getExactUrlResourceList();
//        List<SimpleGrantedAuthority> luterPerms = new ArrayList<>();
//        luterPerms.add(new SimpleGrantedAuthority(resources.get(0).getPerm()));
//        luterPerms.add(new SimpleGrantedAuthority(resources.get(1).getPerm()));
//        luterPerms.add(new SimpleGrantedAuthority(resources.get(2).getPerm()));
//        luterPerms.add(new SimpleGrantedAuthority(resources.get(6).getPerm()));
//        luterPerms.add(new SimpleGrantedAuthority(resources.get(7).getPerm()));
//        luterPerms.add(new SimpleGrantedAuthority(resources.get(8).getPerm()));
//        luterPerms.add(new SimpleGrantedAuthority(resources.get(9).getPerm()));
//        luterPerms.add(new SimpleGrantedAuthority(resources.get(10).getPerm()));
//        List<SimpleGrantedAuthority> adminPerms =
//                resources.stream().map(d -> new SimpleGrantedAuthority(d.getPerm())).collect(Collectors.toList());
//        switch (principal) {
//            case "APP:1":
//                return adminPerms;
//            case "APP:2":
//                return luterPerms;
//            case "PC:1":
//                return adminPerms;
//            case "PC:2":
//                return luterPerms;
//            default:
//                return new ArrayList<>();
//        }
//    }
//
//    public static String encode(String plainPassword) {
//        if (StrUtils.isEmpty(plainPassword)) {
//            throw new HeimdallException("密码不能为空");
//        }
//        return Md5Util.encrypt(plainPassword);
//    }
//
//    public static boolean matches(String plainPassword, String cipherPassword) {
//        if (StrUtils.isEmpty(plainPassword) || StrUtils.isEmpty(cipherPassword)) {
//            throw new HeimdallException("明文密码或者密文密码均不能为空");
//        }
//        return Md5Util.matches(plainPassword, cipherPassword);
//    }
//
//}
