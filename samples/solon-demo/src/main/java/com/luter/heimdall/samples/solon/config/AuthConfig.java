/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.samples.solon.config;

import com.luter.heimdall.core.authorization.provider.DocumentAuthorityDataProvider;
import com.luter.heimdall.core.authorization.store.AuthorizationStore;
import com.luter.heimdall.core.authorization.store.MemoryCachedAuthorizationStore;
import com.luter.heimdall.core.context.WebContextHolder;
import com.luter.heimdall.core.manager.AuthenticationManager;
import com.luter.heimdall.core.manager.AuthorizationManager;
import com.luter.heimdall.core.manager.HeimdallAuthenticationManager;
import com.luter.heimdall.core.manager.HeimdallAuthorizationManager;
import com.luter.heimdall.core.token.store.MemoryCachedTokenStore;
import com.luter.heimdall.core.token.store.TokenStore;
import com.luter.heimdall.starter.solon.filter.HeimdallAuthorizeFilter;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;

@Configuration
public class AuthConfig {

    @Inject
    private WebContextHolder webContextHolder;

    @Bean
    public AuthorizationManager authorizationManager() {
        final TokenStore tokenStore = new MemoryCachedTokenStore();
        final AuthorizationStore memoryCachedAuthorizationStore = new MemoryCachedAuthorizationStore()
                .dataProvider(new DocumentAuthorityDataProvider());
        final AuthenticationManager authenticationManager =
                new HeimdallAuthenticationManager(webContextHolder)
                        .tokenStore(tokenStore);
        return new HeimdallAuthorizationManager(memoryCachedAuthorizationStore, authenticationManager);
    }

    @Bean
    public HeimdallAuthorizeFilter heimdallAuthorizeFilter() {
        return new HeimdallAuthorizeFilter();
    }
}
