/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.samples.solon.config;

import com.luter.heimdall.core.exception.HeimdallUnauthenticatedException;
import com.luter.heimdall.core.exception.HeimdallUnauthorizedException;
import lombok.extern.slf4j.Slf4j;
import org.noear.solon.annotation.Component;
import org.noear.solon.core.event.EventListener;
import org.noear.solon.core.handle.Context;

@Component
@Slf4j
public class GlobalException implements EventListener<Throwable> {
    @Override
    public void onEvent(Throwable e) {
        e.printStackTrace();
        log.error(e.getMessage());
        Context c = Context.current();
        c.result = new ErrorMsg().setMsg(e.getMessage()).setCode(500);
        c.status(500);
        if (e instanceof HeimdallUnauthenticatedException) {
            c.status(401);
        }
        if (e instanceof HeimdallUnauthorizedException) {
            c.status(403);
        }

    }
}