/*
 *
 *  *    Copyright 2020-2021 luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.samples.webflux.webfluxdemo.config;

import com.luter.heimdall.core.exception.HeimdallExpiredTokenException;
import com.luter.heimdall.core.exception.HeimdallUnauthenticatedException;
import com.luter.heimdall.core.exception.HeimdallUnauthorizedException;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.reactive.error.DefaultErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.server.ResponseStatusException;

import java.util.Map;

@Component
public class GlobalErrorAttributes extends DefaultErrorAttributes {

    @Override
    public Map<String, Object> getErrorAttributes(ServerRequest request, ErrorAttributeOptions options) {
        final Map<String, Object> errorAttributes = super.getErrorAttributes(request, options);
        errorAttributes.put("author", "luter.me");
        final Throwable throwable = getError(request);

        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        if (throwable instanceof HeimdallUnauthenticatedException
                || throwable instanceof HeimdallExpiredTokenException

        ) {
            status = HttpStatus.UNAUTHORIZED;
        }
        if (throwable instanceof HeimdallUnauthorizedException) {
            status = HttpStatus.FORBIDDEN;
        }
        if (throwable instanceof ResponseStatusException) {
            ResponseStatusException exception = (ResponseStatusException) throwable;
            status = exception.getStatus();
        }
        errorAttributes.put("status", status.value());
        return errorAttributes;
    }
}

