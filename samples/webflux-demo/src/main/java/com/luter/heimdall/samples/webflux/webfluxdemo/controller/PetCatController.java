/*
 *
 *  *    Copyright 2020-2021 luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.samples.webflux.webfluxdemo.controller;


import com.luter.heimdall.samples.webflux.webfluxdemo.dto.CatVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Pet cat controller.
 *
 * @author Luter
 */
@RestController
@Slf4j
@RequestMapping("/pet/cat")
public class PetCatController {
    /**
     * Save string.
     *
     * @return the string
     */
    @PostMapping
    public CatVO save(@RequestBody CatVO params) {
        return params;
    }

    /**
     * Delete string.
     *
     * @param id the id
     * @return the string
     */
    @DeleteMapping("/{id}")
    public String delete(@PathVariable Long id) {
        return "delete:" + id;
    }

    /**
     * Update string.
     *
     * @return the string
     */
    @PutMapping
    public CatVO update(@RequestBody CatVO params) {
        return params;
    }

    /**
     * Detail string.
     *
     * @param id the id
     * @return the string
     */
    @GetMapping("/{id}")
    public CatVO detail(@PathVariable Long id) {
        return new CatVO().setName("随机的" + Math.random() * 1000).setGender("女").setId(id).setAge(11);
    }

    /**
     * List string.
     *
     * @return the string
     */
    @GetMapping
    public List<CatVO> list() {
        List<CatVO> cats = new ArrayList<>();
        for (int i = 1; i < 10; i++) {
            cats.add(new CatVO().setAge(i).setName("随机的" + Math.random() * 1000).setGender("女").setId((long) i));
        }
        return cats;
    }

}
