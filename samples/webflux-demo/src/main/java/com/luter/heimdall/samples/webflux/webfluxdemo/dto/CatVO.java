package com.luter.heimdall.samples.webflux.webfluxdemo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class CatVO implements Serializable {

    private Long id;
    private String name;
    private String gender;
    private Integer age;

}
