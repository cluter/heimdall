/*
 *
 *  *    Copyright 2020-2021 luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.samples.webflux.webfluxdemo.service;

import com.luter.heimdall.core.authorization.authority.GrantedAuthority;
import com.luter.heimdall.core.authorization.authority.MethodAndUrlGrantedAuthority;
import com.luter.heimdall.core.exception.HeimdallException;
import com.luter.heimdall.core.utils.StrUtils;
import com.luter.heimdall.core.utils.crypto.Md5Util;
import com.luter.heimdall.samples.webflux.webfluxdemo.dto.SysResourceDTO;
import com.luter.heimdall.samples.webflux.webfluxdemo.dto.SysUserDTO;
import org.springframework.http.HttpMethod;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 模拟数据工具类
 *
 * @author Luter
 */
public final class DataUtil {

    /**
     * The constant CIPHER_PASSWORD.
     */
    public static final String CIPHER_PASSWORD = encode("aaaaaa");

    /**
     * 模拟 用户 表数据
     *
     * @return the simple user list
     */
    public static List<SysUserDTO> getUserList() {
        List<SysUserDTO> userList = new ArrayList<>();
        //系统用户
        userList.add(new SysUserDTO("APP", 1L, "admin", "17777777777", CIPHER_PASSWORD, true));
        userList.add(new SysUserDTO("APP", 2L, "luter", "18888888888", CIPHER_PASSWORD, true));
        return userList;
    }

    /**
     * 模拟 系统资源表 数据
     * <p>
     * 生成一批 restful 风格 资源
     * Method+url 不能重复
     *
     * @return the restful resource list
     */
    private static List<SysResourceDTO> getRestfulResourceList() {
        List<SysResourceDTO> resourceList = new ArrayList<>();
        resourceList.add(new SysResourceDTO(1L, "新增数据", HttpMethod.POST.name(), "/pet/cat", ""));
        resourceList.add(new SysResourceDTO(2L, "查看列表", HttpMethod.GET.name(), "/pet/cat", ""));
        resourceList.add(new SysResourceDTO(3L, "删除数据", HttpMethod.DELETE.name(), "/et/cat/*", ""));
        resourceList.add(new SysResourceDTO(4L, "修改数据", HttpMethod.PUT.name(), "/pet/cat", ""));
        resourceList.add(new SysResourceDTO(5L, "查看详情", HttpMethod.GET.name(), "/pet/cat/*", ""));
        return resourceList;
    }

    /**
     * Gets restful user list.
     *
     * @return the restful user list
     */
    public static Map<String, List<String>> getRestfulPerm() {
        final List<SysResourceDTO> resources = DataUtil.getRestfulResourceList();
        Map<String, List<String>> perms = new LinkedHashMap<>();
        //实际使用的时候，需要对 url 和 perm 进行校验
        for (SysResourceDTO resource : resources) {
            //restful 形式下的权限、角色标志无意义，一句 method+url 进行授权
            perms.put(resource.getUrl(), null);
        }
        return perms;
    }

    /**
     * Gets user restful perm.
     *
     * @param principal the principal
     * @return the user restful perm
     */
    public static List<? extends GrantedAuthority> getUserRestfulPerm(String principal) {
        final List<SysResourceDTO> resources = DataUtil.getRestfulResourceList();
        List<MethodAndUrlGrantedAuthority> luterPerms = new ArrayList<>();
        luterPerms.add(new MethodAndUrlGrantedAuthority(resources.get(0).getMethod(), resources.get(0).getUrl()));
        luterPerms.add(new MethodAndUrlGrantedAuthority(resources.get(1).getMethod(), resources.get(1).getUrl()));
        List<MethodAndUrlGrantedAuthority> adminPerms =
                resources.stream().map(d -> new MethodAndUrlGrantedAuthority(d.getMethod(), d.getUrl())).collect(Collectors.toList());
        switch (principal) {
            case "APP:1":
                return adminPerms;
            case "APP:2":
                return luterPerms;
            case "PC:1":
                return adminPerms;
            case "PC:2":
                return luterPerms;
            default:
                return new ArrayList<>();
        }
    }

    /**
     * Encode string.
     *
     * @param plainPassword the plain password
     * @return the string
     */
    public static String encode(String plainPassword) {
        if (StrUtils.isEmpty(plainPassword)) {
            throw new HeimdallException("密码不能为空");
        }
        return Md5Util.encrypt(plainPassword);
    }

    /**
     * Matches boolean.
     *
     * @param plainPassword  the plain password
     * @param cipherPassword the cipher password
     * @return the boolean
     */
    public static boolean matches(String plainPassword, String cipherPassword) {
        if (StrUtils.isEmpty(plainPassword) || StrUtils.isEmpty(cipherPassword)) {
            throw new HeimdallException("明文密码或者密文密码均不能为空");
        }
        return encode(plainPassword).equals(cipherPassword);
    }

}
