/*
 *
 *  *    Copyright 2020-2021 luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.samples.webflux.webfluxdemo.service;

import com.luter.heimdall.core.authorization.authority.GrantedAuthority;
import com.luter.heimdall.core.authorization.provider.AuthorityDataProvider;
import com.luter.heimdall.core.details.UserDetails;

import java.util.List;
import java.util.Map;

/**
 * 权限提供服务
 * <p>
 * 真实环境需要从数据库等其他地方获取
 *
 * @author luter
 */
public class SimulatorDataProvider implements AuthorityDataProvider {
    @Override
    public Map<String, List<String>> loadAppAuthorities(String appId) {
        return DataUtil.getRestfulPerm();
    }

    @Override
    public List<? extends GrantedAuthority> loadUserAuthorities(UserDetails userDetails) {
        return DataUtil.getUserRestfulPerm(userDetails.getPrincipal());
    }
}
