/*
 *
 *  *    Copyright 2020-2021 luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.samples.webflux.webfluxdemo.service;

import com.luter.heimdall.core.exception.HeimdallException;
import com.luter.heimdall.samples.webflux.webfluxdemo.dto.SysUserDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 模拟获取用户服务
 * <p>
 * 真实环境需要从数据库等其他地方获取
 *
 * @author luter
 */
@Slf4j
@Service
public class SysUserService {
    /**
     * 模拟通过用户名从数据库查询用户
     *
     * @param username the username
     * @return the user by name
     */
    public SysUserDTO getUserByName(String username) {
        //此处用模拟数据模拟从数据库根据username获取用户信息
        List<SysUserDTO> findUserList = DataUtil.getUserList()
                .stream()
                .filter(item -> item.getUsername().equals(username))
                .collect(Collectors.toList());
        if (findUserList.isEmpty()) {
            throw new HeimdallException("用户名或者密码错误");
        }
        //这里可以对用户状态等信息进行校验
        return findUserList.get(0);
    }
}
