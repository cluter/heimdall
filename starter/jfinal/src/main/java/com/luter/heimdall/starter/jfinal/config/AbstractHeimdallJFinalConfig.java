package com.luter.heimdall.starter.jfinal.config;

import com.jfinal.config.*;
import com.jfinal.template.Engine;
import com.luter.heimdall.starter.jfinal.interceptor.HeimdallWebContextInterceptor;

/**
 * 基础配置
 * <p>
 * 如果不想继承此类，就需要自己手动注册：HeimdallWebContextInterceptor 拦截器
 *
 * @author luter
 */
public class AbstractHeimdallJFinalConfig extends JFinalConfig {
    @Override
    public void configConstant(Constants constants) {

    }

    @Override
    public void configRoute(Routes routes) {

    }

    @Override
    public void configEngine(Engine engine) {

    }

    @Override
    public void configPlugin(Plugins plugins) {

    }

    @Override
    public void configInterceptor(Interceptors interceptors) {
        //web context 上下文
        interceptors.addGlobalActionInterceptor(new HeimdallWebContextInterceptor());
    }

    @Override
    public void configHandler(Handlers handlers) {

    }
}
