package com.luter.heimdall.starter.jfinal.context;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The type J final servlet request utils.
 *
 * @author luter
 */
public class JFinalServletRequestUtils {

    /**
     * The constant REQUEST_THREAD_LOCAL.
     */
    private static final ThreadLocal<HttpServletRequest> REQUEST_THREAD_LOCAL = new InheritableThreadLocal<>();
    /**
     * The constant RESPONSE_THREAD_LOCAL.
     */
    private static final ThreadLocal<HttpServletResponse> RESPONSE_THREAD_LOCAL = new InheritableThreadLocal<>();

    /**
     * Gets request.
     *
     * @return the request
     */
    public static HttpServletRequest getRequest() {
        return REQUEST_THREAD_LOCAL.get();
    }

    /**
     * Gets response.
     *
     * @return the response
     */
    public static HttpServletResponse getResponse() {
        return RESPONSE_THREAD_LOCAL.get();
    }

    /**
     * Sets request.
     *
     * @param request the request
     */
    public static void setRequest(HttpServletRequest request) {
        REQUEST_THREAD_LOCAL.set(request);
    }

    /**
     * Sets response.
     *
     * @param response the response
     */
    public static void setResponse(HttpServletResponse response) {
        RESPONSE_THREAD_LOCAL.set(response);
    }

    /**
     * Remove request.
     */
    public static void removeRequest() {
        REQUEST_THREAD_LOCAL.remove();
    }

    /**
     * Remove response.
     */
    public static void removeResponse() {
        RESPONSE_THREAD_LOCAL.remove();
    }

    /**
     * Clear.
     */
    public static void clear() {
        removeRequest();
        removeResponse();
    }
}
