package com.luter.heimdall.starter.jfinal.context;
 
import com.luter.heimdall.core.context.WebContextHolder;
import com.luter.heimdall.core.context.WebRequestHolder;
import com.luter.heimdall.core.context.WebResponseHolder;
import com.luter.heimdall.core.exception.HeimdallException;
import com.luter.heimdall.servlet.context.ServletWebRequestHolder;
import com.luter.heimdall.servlet.context.ServletWebResponseHolder;
import org.slf4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * The type J final web context holder.
 *
 * @author luter
 */
public class JFinalWebContextHolder implements WebContextHolder {

    /**
     * The constant log.
     */
    private static final transient Logger log = getLogger(JFinalWebContextHolder.class);

    @Override
    public WebRequestHolder getRequest() {
        final HttpServletRequest request = JFinalServletRequestUtils.getRequest();
        if (null == request) {
            throw new HeimdallException("获取Web 上下文失败:HttpServletRequest");
        }
        return new ServletWebRequestHolder(request);
    }

    @Override
    public WebResponseHolder getResponse() {
        final HttpServletResponse response = JFinalServletRequestUtils.getResponse();
        if (null == response) {
            throw new HeimdallException("获取Web 上下文失败:HttpServletResponse");
        }
        return new ServletWebResponseHolder(response);
    }
}
