package com.luter.heimdall.starter.jfinal.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.luter.heimdall.core.authorization.handler.HeimdallMethodAuthorizationHandler;
import com.luter.heimdall.core.manager.AuthorizationManager;

import java.lang.reflect.Method;

/**
 * The type Heimdall j final annotation interceptor.
 *
 * @author luter
 */
public class HeimdallJFinalAnnotationInterceptor implements Interceptor {
    /**
     * The Authorization manager.
     */
    private final AuthorizationManager authorizationManager;

    /**
     * Instantiates a new Heimdall j final annotation interceptor.
     *
     * @param authorizationManager the authorization manager
     */
    public HeimdallJFinalAnnotationInterceptor(AuthorizationManager authorizationManager) {
        this.authorizationManager = authorizationManager;
    }

    @Override
    public void intercept(Invocation invocation) {
        final Method method = invocation.getMethod();
        HeimdallMethodAuthorizationHandler.handleMethodAnnotation(authorizationManager, method);
        invocation.invoke();
    }

}
