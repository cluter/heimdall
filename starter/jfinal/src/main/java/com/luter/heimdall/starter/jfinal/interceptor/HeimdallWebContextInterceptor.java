package com.luter.heimdall.starter.jfinal.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.luter.heimdall.starter.jfinal.context.JFinalServletRequestUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * web 上下文注入拦截器，持有当前 request 和 response
 * <p>
 * <p>
 * 一次请求发生：
 * <p>
 * 请求开始，把 request 和 response 拿出来，放入 ThreadLocal 中。
 * <p>
 * 请求结束，清理 ThreadLocal。
 *
 * @author luter
 */
public class HeimdallWebContextInterceptor implements Interceptor {
    @Override
    public void intercept(Invocation inv) {
        final HttpServletRequest request = inv.getController().getRequest();
        final HttpServletResponse response = inv.getController().getResponse();
        JFinalServletRequestUtils.setRequest(request);
        JFinalServletRequestUtils.setResponse(response);
        inv.invoke();
        JFinalServletRequestUtils.clear();
    }
}