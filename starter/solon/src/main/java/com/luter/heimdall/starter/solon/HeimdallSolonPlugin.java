/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.starter.solon;

import com.luter.heimdall.core.annotation.*;
import com.luter.heimdall.core.context.WebContextHolder;
import com.luter.heimdall.starter.solon.annotation.HeimdallSolonAnnotationInterceptor;
import com.luter.heimdall.starter.solon.context.SolonWebContextHolder;
import org.noear.solon.SolonApp;
import org.noear.solon.core.Aop;
import org.noear.solon.core.Plugin;

/**
 * The type Heimdall solon plugin.
 *
 * @author luter
 */
public class HeimdallSolonPlugin implements Plugin {
    @Override
    public void start(SolonApp app) {
        //上下文持有注册
        Aop.wrapAndPut(WebContextHolder.class, new SolonWebContextHolder());

        //注解注册
        final HeimdallSolonAnnotationInterceptor me = HeimdallSolonAnnotationInterceptor.ME;
        Aop.context().beanAroundAdd(RequiresUser.class, me, 0);
        Aop.context().beanAroundAdd(RequiresRole.class, me, 1);
        Aop.context().beanAroundAdd(RequiresRoles.class, me, 2);
        Aop.context().beanAroundAdd(RequiresPermission.class, me, 3);
        Aop.context().beanAroundAdd(RequiresPermissions.class, me, 4);
    }

    @Override
    public void prestop() throws Throwable {
        Plugin.super.prestop();
    }

    @Override
    public void stop() throws Throwable {
        Plugin.super.stop();
    }
}
