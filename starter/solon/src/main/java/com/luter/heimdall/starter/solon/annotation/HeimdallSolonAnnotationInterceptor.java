/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.starter.solon.annotation;

import com.luter.heimdall.core.authorization.handler.HeimdallMethodAuthorizationHandler;
import com.luter.heimdall.core.manager.AuthorizationManager;
import org.noear.solon.core.Aop;
import org.noear.solon.core.aspect.Interceptor;
import org.noear.solon.core.aspect.Invocation;
import org.slf4j.Logger;

import java.lang.reflect.Method;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * The type Heimdall solon annotation interceptor.
 *
 * @author luter
 */
public class HeimdallSolonAnnotationInterceptor implements Interceptor {
    /**
     * The constant log.
     */
    private static final transient Logger log = getLogger(HeimdallSolonAnnotationInterceptor.class);
    /**
     * The constant ME.
     */
    public static final HeimdallSolonAnnotationInterceptor ME = new HeimdallSolonAnnotationInterceptor();

    /**
     * Instantiates a new Heimdall solon annotation interceptor.
     */
    public HeimdallSolonAnnotationInterceptor() {
    }

    @Override
    public Object doIntercept(Invocation inv) throws Throwable {
        final AuthorizationManager authorizationManager = Aop.context().getBean(AuthorizationManager.class);
        final Method method = inv.method().getMethod();
        if (null != authorizationManager) {
            HeimdallMethodAuthorizationHandler.handleMethodAnnotation(authorizationManager, method);
        } else {
            log.error("AuthorizationManager is null.HeimdallSolonAnnotationInterceptor initialization failed");
        }

        return inv.invoke();
    }

}
