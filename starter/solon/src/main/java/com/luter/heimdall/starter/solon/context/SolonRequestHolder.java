/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.starter.solon.context;

import com.luter.heimdall.core.context.WebRequestHolder;
import org.noear.solon.core.handle.Context;

/**
 * The type Solon request holder.
 *
 * @author luter
 */
public class SolonRequestHolder implements WebRequestHolder {
    /**
     * The Ctx.
     */
    protected Context ctx;

    /**
     * Instantiates a new Solon request holder.
     */
    public SolonRequestHolder() {
        ctx = Context.current();
    }

    @Override
    public String getCookieValue(String name) {
        return ctx.cookie(name);
    }

    @Override
    public String getHeaderValue(String name) {
        return ctx.header(name);
    }

    @Override
    public String getQueryValue(String name) {
        return ctx.param(name);
    }

    @Override
    public String getRequestURI() {
        return ctx.uri().getPath();
    }

    @Override
    public String getQueryString() {
        return ctx.queryString();
    }

    @Override
    public String getMethod() {
        return ctx.method();
    }

    @Override
    public String getRemoteIp() {
        return ctx.realIp();
    }
}
