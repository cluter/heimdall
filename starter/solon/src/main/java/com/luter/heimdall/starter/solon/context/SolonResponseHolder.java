/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.starter.solon.context;

import com.luter.heimdall.core.context.WebResponseHolder;
import com.luter.heimdall.core.cookie.SimpleCookie;
import org.noear.solon.core.handle.Context;

/**
 * The type Solon response holder.
 *
 * @author luter
 */
public class SolonResponseHolder implements WebResponseHolder {
    /**
     * The Ctx.
     */
    protected Context ctx;

    /**
     * Instantiates a new Solon response holder.
     */
    public SolonResponseHolder() {
        ctx = Context.current();
    }

    @Override
    public WebResponseHolder addCookie(SimpleCookie cookie) {
        setHeader(SimpleCookie.COOKIE_HEADER_NAME, cookie.toString());
        return this;
    }

    @Override
    public WebResponseHolder setStatus(int statusCode) {
        ctx.status(statusCode);
        return this;
    }

    @Override
    public WebResponseHolder setHeader(String name, String value) {
        ctx.headerAdd(name, value);
        return this;
    }
}
