/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.boot.configurer;

import com.luter.heimdall.core.config.ConfigManager;
import com.luter.heimdall.core.config.HeimdallProperties;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * 自动配置
 * <p>
 * 1、配置参数注入
 * <p>
 * 2、请求解析器RequestTokenResolver注入
 *
 * @author luter
 */
public class HeimdallAutoConfiguration {
    /**
     * The constant log.
     */
    private static final transient Logger log = getLogger(HeimdallAutoConfiguration.class);

    /**
     * 全局配置参数注入 ConfigManager
     *
     * @param heimdallProperties the heimdall properties
     */
    @Autowired(required = false)
    @ConditionalOnBean(HeimdallProperties.class)
    public void setConfig(HeimdallProperties heimdallProperties) {
        ConfigManager.setConfig(heimdallProperties);
        log.warn("[HeimdallAutoConfiguration]:: Heimdall Properties Inject Successfully.\nProperties = [{}]",
                ConfigManager.getConfig().toString());
    }

}
