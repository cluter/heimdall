/*
 *
 *  *    Copyright 2020-2021 luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.boot.web.configurer;

import com.luter.heimdall.boot.configurer.HeimdallAutoConfiguration;
import com.luter.heimdall.boot.web.resolver.CurrentTokenRequestArgumentResolver;
import com.luter.heimdall.boot.web.resolver.CurrentUserDetailsRequestArgumentResolver;
import com.luter.heimdall.core.manager.AuthenticationManager;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Web MVC 默认配置
 * <p>
 * 1、注入了当前登录用户请求参数解析器
 *
 * @author luter
 */
@AutoConfigureAfter(HeimdallAutoConfiguration.class)
public abstract class AbstractAuthWebMvcConfigurer implements WebMvcConfigurer {
    private static final transient Logger log = getLogger(AbstractAuthWebMvcConfigurer.class);

    @Autowired
    private AuthenticationManager authenticationManager;

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        WebMvcConfigurer.super.addArgumentResolvers(resolvers);
        resolvers.add(new CurrentTokenRequestArgumentResolver(authenticationManager));
        resolvers.add(new CurrentUserDetailsRequestArgumentResolver(authenticationManager));
        log.info("[AbstractAuthWebMvcConfigurer]:: Default ArgumentResolver initialized");
    }

}
