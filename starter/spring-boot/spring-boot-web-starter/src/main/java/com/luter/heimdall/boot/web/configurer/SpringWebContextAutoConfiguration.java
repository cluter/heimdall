/*
 *
 *  *    Copyright 2020-2021 Luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.boot.web.configurer;

import com.luter.heimdall.boot.web.context.SpringWebContextHolder;
import com.luter.heimdall.core.context.WebContextHolder;
import org.slf4j.Logger;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Spring boot Web 上下文注入 自动配置
 *
 * @author luter
 */
@Configuration
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
public class SpringWebContextAutoConfiguration {
    /**
     * The constant log.
     */
    private static final transient Logger log = getLogger(SpringWebContextAutoConfiguration.class);

    /**
     * Web 上下文
     *
     * @return the web context holder
     */
    @Bean
    @Primary
    public WebContextHolder webContextHolder() {
        log.warn("[SpringWebContextAutoConfiguration]:: Default WebContextHandler Initialized");
        return new SpringWebContextHolder();
    }
}
