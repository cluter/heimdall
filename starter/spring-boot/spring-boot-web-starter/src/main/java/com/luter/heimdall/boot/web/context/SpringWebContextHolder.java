/*
 *
 *  *    Copyright 2020-2021 luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.boot.web.context;

import com.luter.heimdall.core.context.WebContextHolder;
import com.luter.heimdall.core.context.WebRequestHolder;
import com.luter.heimdall.core.context.WebResponseHolder;
import com.luter.heimdall.core.exception.HeimdallException;
import com.luter.heimdall.servlet.context.ServletWebRequestHolder;
import com.luter.heimdall.servlet.context.ServletWebResponseHolder;
import org.slf4j.Logger;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Spring mvc web context 持有者注入
 * <p>
 * 通过 spring factories 机制注入
 *
 * @author luter
 */
public class SpringWebContextHolder implements WebContextHolder {
    private static final transient Logger log = getLogger(SpringWebContextHolder.class);

    @Override
    public WebRequestHolder getRequest() {
        log.debug("[SpringWebContextHolder]:: Default WebRequestHolder Initialized");
        return new ServletWebRequestHolder(getSpringServletRequest());
    }

    @Override
    public WebResponseHolder getResponse() {
        log.debug("[SpringWebContextHolder]:: Default WebResponseHolder Initialized");
        return new ServletWebResponseHolder(getSpringServletResponse());

    }

    /**
     * Gets spring servlet request.
     *
     * @return the spring servlet request
     */
    private HttpServletRequest getSpringServletRequest() {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (null == servletRequestAttributes) {
            throw new HeimdallException("Not a valid HttpServletRequest context," +
                    "ServletWebRequestHolder initialized failed");
        }

        return servletRequestAttributes.getRequest();
    }

    /**
     * Gets spring servlet response.
     *
     * @return the spring servlet response
     */
    private HttpServletResponse getSpringServletResponse() {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (null == servletRequestAttributes) {
            throw new HeimdallException("Not a valid HttpServletResponse context," +
                    "ServletWebResponseHolder initialized failed");
        }
        return servletRequestAttributes.getResponse();
    }

}
