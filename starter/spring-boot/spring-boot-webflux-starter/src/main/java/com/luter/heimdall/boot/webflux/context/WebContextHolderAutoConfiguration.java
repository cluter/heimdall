/*
 *
 *  *    Copyright 2020-2021 luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.boot.webflux.context;

import com.luter.heimdall.core.context.WebContextHolder;
import com.luter.heimdall.webflux.web.WebFluxWebContextHolder;
import org.slf4j.Logger;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * The type Web context holder autoconfiguration.
 * Web 上下文持有者
 *
 * @author luter
 */
@Configuration
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.REACTIVE)
public class WebContextHolderAutoConfiguration {

    /**
     * The constant log.
     */
    private static final transient Logger log = getLogger(WebContextHolderAutoConfiguration.class);

    /**
     * 注册默认 Web 上下文持有者
     *
     * @return the web context holder
     */
    @Bean
    @Primary
    public WebContextHolder webContextHolder() {
        log.warn("[WebContextHolderAutoConfiguration]::Default WebContextHolder initialized");
        return new WebFluxWebContextHolder();
    }
}
