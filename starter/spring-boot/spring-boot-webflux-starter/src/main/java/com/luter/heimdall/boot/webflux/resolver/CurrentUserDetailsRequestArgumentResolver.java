/*
 *
 *  *    Copyright 2020-2021 luter.me
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.luter.heimdall.boot.webflux.resolver;

import com.luter.heimdall.core.annotation.CurrentUserDetails;
import com.luter.heimdall.core.manager.AuthenticationManager;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.springframework.core.MethodParameter;
import org.springframework.web.reactive.BindingContext;
import org.springframework.web.reactive.result.method.HandlerMethodArgumentResolver;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * 实现注解:Current UserDetails
 *
 * @author luter
 */
@RequiredArgsConstructor
public class CurrentUserDetailsRequestArgumentResolver implements HandlerMethodArgumentResolver {
    private static final transient Logger log = getLogger(CurrentUserDetailsRequestArgumentResolver.class);

    private final AuthenticationManager authenticationManager;

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.hasParameterAnnotation(CurrentUserDetails.class);
    }

    @Override
    public Mono<Object> resolveArgument(MethodParameter parameter, BindingContext bindingContext, ServerWebExchange exchange) {
        CurrentUserDetails currentTokenAnno = parameter.getParameterAnnotation(CurrentUserDetails.class);
        if (null != currentTokenAnno) {
            return Mono.justOrEmpty(authenticationManager.getCurrentToken(true).getDetails());
        }
        return Mono.empty();
    }
}
