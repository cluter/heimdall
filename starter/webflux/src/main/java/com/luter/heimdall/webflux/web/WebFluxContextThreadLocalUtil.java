/*
 *
 *  *
 *  *   Copyright 2020-2021 Luter.me
 *  *
 *  *   Licensed under the Apache License, Version 2.0 (the "License");
 *  *   you may not use this file except in compliance with the License.
 *  *   You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *   Unless required by applicable law or agreed to in writing, software
 *  *   distributed under the License is distributed on an "AS IS" BASIS,
 *  *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *   See the License for the specific language governing permissions and
 *  *   limitations under the License.
 *  *
 *
 */

package com.luter.heimdall.webflux.web;

import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;

/**
 * reactive web 请求响应 thread local.
 * <p>
 * 将请求和响应存入 ThreadLocal 便于获取
 *
 * @author luter
 */
public class WebFluxContextThreadLocalUtil {
    /**
     * The constant requestThreadLocal.
     */
    public static ThreadLocal<ServerHttpRequest> requestThreadLocal = new InheritableThreadLocal<>();
    /**
     * The constant responseThreadLocal.
     */
    public static ThreadLocal<ServerHttpResponse> responseThreadLocal = new InheritableThreadLocal<>();


    /**
     * Sets request.
     *
     * @param request the request
     */
    public static void setRequest(ServerHttpRequest request) {
        requestThreadLocal.set(request);
    }

    /**
     * Gets request.
     *
     * @return the request
     */
    public static ServerHttpRequest getRequest() {
        return requestThreadLocal.get();
    }

    /**
     * Sets response.
     *
     * @param request the request
     */
    public static void setResponse(ServerHttpResponse request) {
        responseThreadLocal.set(request);
    }

    /**
     * Gets response.
     *
     * @return the response
     */
    public static ServerHttpResponse getResponse() {
        return responseThreadLocal.get();
    }

    /**
     * Remove request.
     */
    public static void removeRequest() {
        requestThreadLocal.remove();
    }

    /**
     * Remove response.
     */
    public static void removeResponse() {
        responseThreadLocal.remove();
    }

    /**
     * Remove.
     */
    public static void remove() {
        removeRequest();
        removeResponse();
    }

}
